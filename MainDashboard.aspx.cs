﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using System.Data;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MainDashboard : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //SessionCcode = Session["Ccode"].ToString();
        //SessionLcode = Session["Lcode"].ToString();
        //SessionUserName = Session["Usernmdisplay"].ToString();
        //SessionUserID = Session["UserId"].ToString();
        //SessionFinYearCode = Session["FinYearCode"].ToString();
        //SessionFinYearVal = Session["FinYear"].ToString();

        //if (!IsPostBack)
        //{
        //    Page.Title = "CORAL ERP :: Dashboard";
        //    HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_DashBoard"));
        //    li.Attributes.Add("class", "droplink active open");

        //    string SSQL = "";

        //    SSQL = "Select * From Admin_User_Rights where CompCode = '" + Session["Ccode"].ToString() + "' And ";
        //    SSQL = SSQL + " LocCode = '" + Session["Lcode"].ToString() + "' And ";
        //    SSQL = SSQL + " UserName = '" + Session["Usernmdisplay"].ToString() + "' And ModuleID = '1' And MenuID = '1' ";
        //    SSQL = SSQL + " And FormName = 'Dash board' And ViewRights = '1'";

        //    DataTable DTDash = new DataTable();

        //    DTDash = objdata.RptEmployeeMultipleDetails(SSQL);

        //    if (SessionUserID.ToString() == "Scoto")
        //    {
        //        Load_SupplierWise_Purchase();
        //        Load_ItemWise_Purchase();
        //    }
        //    else
        //    {
        //        if (DTDash.Rows.Count > 0)
        //        {
        //            Load_SupplierWise_Purchase();
        //            Load_ItemWise_Purchase();
        //        }
        //    }
            
        //}

    }

    public void Load_SupplierWise_Purchase()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        lblTit1.Visible = true;

        SSQL = "Select Supp_Name,sum(TotalQuantity)Qty,sum(NetAmount)Amount From Std_Purchase_Order_Main ";
        SSQL = SSQL + " Where CCode='" + SessionCcode + "' and LCode='"+ SessionLcode +"' and FinYearCode='"+ SessionFinYearCode +"' ";
        SSQL = SSQL + " Group By Supp_Name ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        chSuppWisePurDet.DataSource = DT;

        chSuppWisePurDet.Series["sriSuppPurDet"].XValueMember = "Supp_Name";
        chSuppWisePurDet.Series["sriSuppPurDet"].YValueMembers = "Amount";
        this.chSuppWisePurDet.Series[0]["PieLabelStyle"] = "Outside";
        this.chSuppWisePurDet.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.chSuppWisePurDet.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.chSuppWisePurDet.Series[0].BorderWidth = 1;
        this.chSuppWisePurDet.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.chSuppWisePurDet.Legends.Add("Legend1");
        this.chSuppWisePurDet.Legends[0].Enabled = true;
        this.chSuppWisePurDet.Legends[0].Docking = Docking.Bottom;
        this.chSuppWisePurDet.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        
        chSuppWisePurDet.DataBind();
    }

    public void Load_ItemWise_Purchase()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        lblTit2.Visible = true;

        SSQL = "Select ItemName,sum(ReceiveQty)Qty from Trans_GoodsReceipt_Sub ";
        SSQL = SSQL + " Where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' ";
        SSQL = SSQL + " Group By ItemName ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        chItemWisePurDet.DataSource = DT;

        chItemWisePurDet.Series["sriItemPurDet"].XValueMember = "ItemName";
        chItemWisePurDet.Series["sriItemPurDet"].YValueMembers = "Qty";
        this.chItemWisePurDet.Series[0]["PieLabelStyle"] = "Outside";
        this.chItemWisePurDet.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.chItemWisePurDet.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.chItemWisePurDet.Series[0].BorderWidth = 1;
        this.chItemWisePurDet.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.chItemWisePurDet.Legends.Add("Legend1");
        this.chItemWisePurDet.Legends[0].Enabled = true;
        this.chItemWisePurDet.Legends[0].Docking = Docking.Bottom;
        this.chItemWisePurDet.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        
        chItemWisePurDet.DataBind();
    }
}