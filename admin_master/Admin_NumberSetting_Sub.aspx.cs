﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Admin_NumberSetting_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionFromName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Number Setting";
            LoadData_ModuleNames();
            LoadData_FormNames();
            
            if (Session["FormName"] == null)
            {
                SessionFromName = "";
            }
            else
            {
                SessionFromName = Session["FormName"].ToString();
                txtFormName.SelectedItem.Text = SessionFromName;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }

    public void LoadData_ModuleNames()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select ModuleID,ModuleName from Admin_Company_Module_Rights where CompCode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LocCode ='" + SessionLcode + "' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlModule.DataSource = DT;

        DataRow dr = DT.NewRow();
        dr["ModuleID"] = "-Select-";
        dr["ModuleName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlModule.DataTextField = "ModuleName";
        ddlModule.DataValueField = "ModuleID";

        ddlModule.DataBind();
    }

    public void LoadData_FormNames()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select FormID,FormName from MstFormName where ModuleName='" + ddlModule.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtFormName.DataSource = DT;

        DataRow dr = DT.NewRow();
        dr["FormID"] = "-Select-";
        dr["FormName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        txtFormName.DataTextField = "FormName";
        txtFormName.DataValueField = "FormID";

        txtFormName.DataBind();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string SysName = getIPAndName.GetName();

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check Form
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstNumberingSetup where FormName='" + txtFormName.SelectedItem.Text + "' and ModuleId='1' And ";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Form Already Saved...');", true);
            }
        }

        // duplicate Check Form Prefix
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstNumberingSetup where Prefix='" + txtPrefix.Text + "' and ModuleId='1' And ";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Perfix Already Saved...');", true);
            }
        }


        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstNumberingSetup Set ModuleId='" + ddlModule.SelectedValue + "',ModuleName='" + ddlModule.SelectedItem.Text + "',";
                SSQL = SSQL + " FormId ='" + txtFormName.SelectedValue + "',FormName='" + txtFormName.SelectedItem.Text + "',";
                SSQL = SSQL + " Prefix ='" + txtPrefix.Text + "',StartNo='" + txtStartNo.Text + "',EndNo='" + txtEndNo.Text + "',";
                SSQL = SSQL + " Suffix='" + txtSuffix.Text + "' Where FormName='" + txtFormName.SelectedItem.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Company Details Updated Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstNumberingSetup(Ccode,Lcode,ModuleId,ModuleName,FormId,FormName,Prefix,StartNo,EndNo,Suffix,";
                SSQL = SSQL + " Username,Status,CreateOn) Values ('" + SessionCcode + "','" + SessionLcode + "','" + ddlModule.SelectedValue + "',";
                SSQL = SSQL + " '" + ddlModule.SelectedItem.Text + "','" + txtFormName.SelectedValue + "','" + txtFormName.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + txtPrefix.Text + "','" + txtStartNo.Text + "','" + txtEndNo.Text + "','" + txtSuffix.Text + "',";
                SSQL = SSQL + " '" + SessionUserName + "','Add',Getdate())";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Company Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            //txt.Enabled = true;

            Response.Redirect("Admin_NumberSetting_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Company Details Already Saved');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtFormName.SelectedItem.Text = "-Select-"; txtFormName.SelectedValue= "-Select-"; txtPrefix.Text = ""; txtEndNo.Text = "";
        txtSuffix.Text = ""; 
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "select * from MstNumberingSetup where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
        SSQL = SSQL + " FormName ='" + txtFormName.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            txtFormName.Enabled= false;
            txtFormName.SelectedItem.Text = DT.Rows[0]["FormName"].ToString();
            txtFormName.SelectedValue = DT.Rows[0]["FormId"].ToString();
            txtPrefix.Text = DT.Rows[0]["Prefix"].ToString();
            txtEndNo.Text = DT.Rows[0]["EndNo"].ToString();
            txtSuffix.Text = DT.Rows[0]["Suffix"].ToString();
            ddlModule.SelectedValue= DT.Rows[0]["ModuleId"].ToString();
            ddlModule.SelectedItem.Text= DT.Rows[0]["ModuleName"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Admin_NumberSetting_Main.aspx");
    }

    protected void txtFormName_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlModule_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadData_FormNames();
    }
}
