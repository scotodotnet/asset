﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Location_Sub.aspx.cs" Inherits="Location_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Location Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Location Code <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtLocCode" runat="server" class="form-control"/>
                                    <span id="DeptCode" class="text-danger"></span>
                                    <asp:RequiredFieldValidator ControlToValidate="txtLocCode" Display="Dynamic" ValidationGroup="Validate_Field" 
                                        class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Loaction Name <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtLocName" runat="server" class="form-control" />
                                    <span id="ShortName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtLocName" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Address 1 <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtAddr1" runat="server" class="form-control" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtAddr1" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Address 2 <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtAddr2" runat="server" class="form-control" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtAddr2" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">City<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtCity" runat="server" class="form-control" />
                                     <asp:RequiredFieldValidator ControlToValidate="txtCity" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">PinCode<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtPinCode" runat="server" class="form-control" />
                                     <asp:RequiredFieldValidator ControlToValidate="txtPinCode" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Phone Code<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtPhoneCode" runat="server" class="form-control" />
                                     <asp:RequiredFieldValidator ControlToValidate="txtPhoneCode" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Phone No<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtPhoneNo" runat="server" class="form-control" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtPhoneNo" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>     
                        </div>
                         
                        <div class="row">

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Mobile Code<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtMobiCode" runat="server" class="form-control" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtMobiCode" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Mobile<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtMobile" runat="server" class="form-control" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtMobile" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>       

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">GST No<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtRegNo" runat="server" class="form-control" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtRegNo" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                            <asp:Button  ID="btnClear" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnClear_Click"/>
                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click"/>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

