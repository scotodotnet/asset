﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Location_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionLCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Location List";

            if (Session["LocCode"] == null)
            {
                SessionLCode = "";
            }
            else
            {
                SessionLCode = Session["LocCode"].ToString();
                txtLocCode.Text = SessionLCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string SysName = getIPAndName.GetName();

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstLocation Where Location_Name='" + txtLocName.Text + "' And Ccode='" + SessionCcode + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
            }
        }

        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstLocation set Location_Name='" + txtLocName.Text + "',Address1='" + txtAddr1.Text + "',Address2='" + txtAddr2.Text + "',";
                SSQL = SSQL + " City='" + txtCity.Text + "',PinCode='" + txtPinCode.Text + "',Std_Code='" + txtPhoneCode.Text + "',";
                SSQL = SSQL + " Phone_No='" + txtPhoneNo.Text + "',Mobile_Code='" + txtMobiCode.Text + "',Mobile_No='" + txtMobile.Text + "',";
                SSQL = SSQL + " Reg_No='" + txtRegNo.Text + "' Where Lcode='" + txtLocCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Location Details Updated Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstLocation(Ccode,Lcode,Location_Name,Address1,Address2,City,PinCode,Std_Code,Phone_No,Mobile_Code,Mobile_No,";
                SSQL = SSQL + " Reg_No,Status,IP,SystemName,User_ID,User_Name,CreateOn)Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + txtLocCode.Text + "','" + txtLocName.Text + "','" + txtAddr1.Text + "','" + txtAddr2.Text + "',";
                SSQL = SSQL + " '" + txtCity.Text + "','" + txtPinCode.Text + "','" + txtPhoneCode.Text + "','" + txtPhoneNo.Text + "',";
                SSQL = SSQL + " '" + txtMobiCode.Text + "','" + txtMobile.Text + "','" + txtRegNo.Text + "','','" + IP + "','" + SysName + "',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "',Getdate())";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Location Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtLocCode.Enabled = true;

            Response.Redirect("Location_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Location Details Already Saved');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Already Saved');", true);
        }
    }

   
    private void Clear_All_Field()
    {
        txtLocCode.ReadOnly = false;
        txtLocCode.Text = "";txtLocName.Text = "";txtAddr1.Text = "";txtAddr2.Text = "";
        txtCity.Text = "";txtPinCode.Text = "";txtPhoneCode.Text = "";txtPhoneNo.Text = "";
        txtMobile.Text = "";txtMobiCode.Text = "";txtRegNo.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstLocation where  Status <> 'Delete' And LCode='" + txtLocCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtLocCode.ReadOnly = true;
            txtLocName.Text = DT.Rows[0]["Location_Name"].ToString();
            txtAddr1.Text = DT.Rows[0]["Address1"].ToString();
            txtAddr2.Text = DT.Rows[0]["Address2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtPinCode.Text = DT.Rows[0]["PinCode"].ToString();
            txtPhoneCode.Text = DT.Rows[0]["Std_Code"].ToString();
            txtPhoneNo.Text = DT.Rows[0]["Phone_No"].ToString();
            txtMobile.Text = DT.Rows[0]["Mobile_No"].ToString();
            txtMobiCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtRegNo.Text = DT.Rows[0]["Reg_No"].ToString();
           
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Location_Main.aspx");
    }
}
    