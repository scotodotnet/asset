﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Admin_NumberSetting_Main.aspx.cs" Inherits="Admin_NumberSetting_Main" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
    <section class="content">
                <div class="row">
                    <div class="col-md-2">
                        <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom"  runat="server" Text="Add New" OnClick="btnAddNew_Click"/>

                    </div>
                    <!-- /.col -->
                    </div>
                <!--/.row-->
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Number Settings</span></h3>
                                <div class="box-tools pull-right">
                                    <div class="has-feedback">
                                        <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <div class="table-responsive mailbox-messages">
                                    <div class="col-md-12" runat="server" style="padding-top:15px">
					                    <div class="row">
					                        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
					                            <HeaderTemplate>
                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>S. No</th>
                                                                <th>Form Name</th>
                                                                <th>Prefix</th>
                                                                <%--<th>StartNo</th>--%>
                                                                <th>Last Number</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("FormName")%></td>
                                                        <td><%# Eval("Prefix")%></td>
                                                        <%--<td><%# Eval("StartNo")%></td>--%>
                                                        <td><%# Eval("EndNo")%></td>
                                                        <td runat="server" visible="false"><%# Eval("Suffix")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                                Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("FormName")%>'>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnViewGrid" class="btn btn-primary btn-sm fa fa-list" Visible="false"  runat="server" 
                                                                Text="" OnCommand="GridViewEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("FormName")%>'>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  Visible="false" runat="server" 
                                                                Text="" CommandArgument="Delete" OnCommand="GridDeleteEnquiryClick" CommandName='<%# Eval("FormName")%>' 
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Company details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>                                
					                        </asp:Repeater>
					                    </div>
					                </div><!-- /.table -->
                                </div><!-- /.mail-box-messages -->
                            </div><!-- /.box-body -->
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section>
        </div>
</asp:Content>

