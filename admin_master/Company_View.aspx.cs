﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Company_View : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionCCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Company List";

            if (Session["CompCode"] == null)
            {
                SessionCCode = "";
            }
            else
            {
                SessionCCode = Session["CompCode"].ToString();
                txtCompCode.Text = SessionCCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string SysName = getIPAndName.GetName();

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstCompany Where CompanyName='" + txtCompName.Text + "' And Ccode='" + SessionCcode + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
            }
        }

        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstCompany set CompanyName='" + txtCompName.Text + "',Address1='" + txtAddr1.Text + "',Address2='" + txtAddr2.Text + "',";
                SSQL = SSQL + " City='" + txtCity.Text + "',PinCode='" + txtPinCode.Text + "',State='" + txtState.Text + "',Country='" + txtCountry.Text + "',";
                SSQL = SSQL + " PhoneCode='" + txtPhoneCode.Text + "',PhoneNo='" + txtPhoneNo.Text + "',Mobile='" + txtMobile.Text + "',";
                SSQL = SSQL + " MailID='" + txtMail.Text + "',TinNo='" + txtTin.Text + "',GSTNo='" + txtGst.Text + "',PanNo='" + txtPan.Text + "',";
                SSQL = SSQL + " Others='"+ txtOthers.Text +"' Where CCode='" + txtCompCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Company Details Updated Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstCompany(Ccode,CompanyName,Address1,Address2,City,PinCode,State,Country,PhoneCode,PhoneNo,MobileCode,Mobile,";
                SSQL = SSQL + " MailID,TinNo,GSTNo,PanNo,Others,IP,SystemName,UserID,UserName,CreateOn,Status)Values(";
                SSQL = SSQL + " '" + txtCompCode.Text + "','" + txtCompName.Text + "','" + txtAddr1.Text + "','" + txtAddr2.Text + "',";
                SSQL = SSQL + " '" + txtCity.Text + "','" + txtPinCode.Text + "','" + txtState.Text + "','" + txtCountry.Text + "',";
                SSQL = SSQL + " '" + txtPhoneCode.Text + "','" + txtPhoneNo.Text + "','+91','" + txtMobile.Text + "','" + txtMail.Text + "',";
                SSQL = SSQL + " '" + txtTin.Text + "','" + txtGst.Text + "','" + txtPan.Text + "','" + txtOthers.Text + "','" + IP + "','" + SysName + "',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "',Getdate(),'')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Company Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtCompCode.Enabled = true;

            Response.Redirect("Company_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Company Details Already Saved');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtCompCode.ReadOnly = false;
        txtCompCode.Text = "";txtCompName.Text = "";txtAddr1.Text = "";txtAddr2.Text = "";
        txtCity.Text = "";txtPinCode.Text = "";txtState.Text = "";txtCountry.Text = "";txtPhoneCode.Text = "";txtPhoneNo.Text = "";
        txtMobile.Text = "";txtMail.Text = "";txtTin.Text = "";txtGst.Text = "";txtOthers.Text = "";txtPan.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstCompany where  Status <> 'Delete' And CCode='" + txtCompCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtCompCode.ReadOnly = true;
            txtCompName.Text = DT.Rows[0]["CompanyName"].ToString();
            txtAddr1.Text = DT.Rows[0]["Address1"].ToString();
            txtAddr2.Text = DT.Rows[0]["Address2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtPinCode.Text = DT.Rows[0]["PinCode"].ToString();
            txtState.Text = DT.Rows[0]["State"].ToString();
            txtCountry.Text = DT.Rows[0]["Country"].ToString();
            txtPhoneCode.Text = DT.Rows[0]["PhoneCode"].ToString();
            txtPhoneNo.Text = DT.Rows[0]["PhoneNo"].ToString();
            txtMobile.Text = DT.Rows[0]["Mobile"].ToString();
            txtMail.Text = DT.Rows[0]["MailID"].ToString();
            txtTin.Text = DT.Rows[0]["TinNo"].ToString();
            txtGst.Text = DT.Rows[0]["GSTNo"].ToString();
            txtOthers.Text = DT.Rows[0]["PanNo"].ToString();
            txtPan.Text = DT.Rows[0]["Others"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Company_Main.aspx");
    }
}
