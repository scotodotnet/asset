﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Mst_Designation_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionDesigCode = "";
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Designation";
            Load_Department();

            if (Session["DesigCode"] == null)
            {
                SessionDesigCode = "";
            }
            else
            {
                SessionDesigCode = Session["DesigCode"].ToString();
                txtDesigCode.Text = SessionDesigCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }

    private void Load_Department()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode +"' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status='Add' ";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlDeptName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        if (btnSave.Text == "Save")
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Designation Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Adding New Designation...');", true);
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Designation Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtDesigCode.Text = Auto_Transaction_No;
                }
            }
        }


        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstDesignation Where DesigName='" + txtDesigName.Text + "' And Ccode ='" + SessionCcode + "' And ";
            SSQL = SSQL + " DeptName='" + ddlDeptName.SelectedItem.Text + "' And  Lcode ='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Designation Name Already Exsit');", true);
            }
        }

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstDesignation set DesigName='" + txtDesigName.Text + "',DeptCode='" + ddlDeptName.SelectedValue + "',";
                SSQL = SSQL + " DeptName='" + ddlDeptName.SelectedItem.Text + "' Where Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And Lcode ='" + SessionLcode + "' And DesigCode='" + txtDesigCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Designation Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstDesignation(Ccode,Lcode,DesigCode,DesigName,DeptCode,DeptName,Status,UserID,UserName,CreateOn) Values( ";
                SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + txtDesigCode.Text.ToUpper() + "','" + txtDesigName.Text + "',";
                SSQL = SSQL + " '" + ddlDeptName.SelectedValue + "','" + ddlDeptName.SelectedItem.Text + "','ADD','" + SessionUserID + "',";
                SSQL = SSQL + " '" + SessionUserName + "',Getdate())";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtDesigCode.Enabled = true;

            Response.Redirect("Mst_Designation_Main.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Already Saved');", true);
        }
    }
    private void Clear_All_Field()
    {
        //txtDesigCode.ReadOnly = false;
        ddlDeptName.SelectedItem.Text = "-Select-";
        ddlDeptName.SelectedValue = "-Select-";
        txtDesigCode.Text = "";
        txtDesigName.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstDesignation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status = 'ADD' And ";
        SSQL = SSQL + " DesigCode='" + txtDesigCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            //txtDesigCode.ReadOnly = true;
            txtDesigName.Text = DT.Rows[0]["DesigName"].ToString();
            ddlDeptName.SelectedItem.Text = DT.Rows[0]["DeptName"].ToString();
            ddlDeptName.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Mst_Designation_Main.aspx");
    }
}
