﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Mst_UOM_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Unit Of Measurement List";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_Enquiry_Grid();
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "UOM Type Master");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Add...');", true);
        }
        else
        {
            //Session.Remove("Pur_RequestNo_Approval");
            //Session.Remove("Pur_Request_No_Amend_Approval");
            Session.Remove("UOMCode");
            Response.Redirect("MstUOM_Sub.aspx");
        }
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "UOM Type Master");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Edit...');", true);
        }

        if (!ErrFlag)
        {
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("UOMCode");
            Session["UOMCode"] = Enquiry_No_Str;
            Response.Redirect("MstUOM_Sub.aspx");
        }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "UOM Type Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Delete...');", true);
        }
        //User Rights Check End
         
        if (!ErrFlag)
        {
            DataTable dtdpurchase = new DataTable();
            DataTable DT = new DataTable();
            SSQL = "Select * from MstUOM where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " UOMCode='" + e.CommandName.ToString() + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                SSQL = "Update MstUOM Set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And UOMCode='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Unit Of Measurement Details Deleted Successfully...');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unit Of Measurement Details Deleted Successfully');", true);
                Load_Data_Enquiry_Grid();
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select * from MstUOM where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status = 'ADD'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}