﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstDepartment.aspx.cs" Inherits="master_forms_MstDepartment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i> Department Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Department Code  <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtDeptCode" runat="server" class="form-control"/>
                                    <span id="DeptCode" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtDeptCode" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Department Name  <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtDeptName" runat="server" class="form-control" />
                                    <span id="DeptName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtDeptName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-2">
                                <div class="form-group" style="align-content:center">
                                    <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save"  ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                </div>
                            </div>
                            <div class="col-md-5"></div>
                        </div>
                            
                        <div class="box-body no-padding">
                            <div class="table-responsive mailbox-messages">
                                <asp:Repeater ID="GrdDepartment" runat="server">
			                        <HeaderTemplate>
                                        <table  class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>S. No</th>
                                                    <th>Department Code</th>
                                                    <th>Department Name</th>
                                                    <th>Mode</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Container.ItemIndex + 1 %></td>
                                            <td><%# Eval("DeptCode")%></td>
                                            <td><%# Eval("DeptName")%></td>
                                            <td>
                                                <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                    Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("DeptCode")%>'>
                                                </asp:LinkButton>

                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                    Text="" CommandArgument="Delete" OnCommand="GridDeleteClick" CommandName='<%# Eval("DeptCode")%>' 
                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>                                
			                    </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

