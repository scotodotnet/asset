﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstAssetTypeSub.aspx.cs" Inherits="Master_MstAssetTypeSub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Asset Type</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label>AssetType Code</label>
                                                <asp:TextBox runat="server" ID="txtAssettypeCode" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAssettypeCode" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>AssetType Name</label>
                                                <asp:TextBox runat="server" ID="txtAssettypeName" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAssettypeName" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="box-header with-border">
                                                <h3 class="box-title">Sub Type </h3>
                                            </div>

                                            <div class="clearfix"></div>
                                            <br />

                                            <div class="box-body no-padding">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Asset Sub Type Code</label>
                                                            <asp:TextBox runat="server" ID="txtSubTypeCode" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtSubTypeCode" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                                class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                                                ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Asset Sub Type Name</label>
                                                            <asp:TextBox runat="server" ID="txtAssetSubTypeName" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtAssetSubTypeName" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                                class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true"
                                                                ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button ID="btnAdd" class="btn btn-success" runat="server" Text="Add" ValidationGroup="Validate_Field" OnClick="btnAdd_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="true">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>S. No</th>
                                                                <th>Asset Type</th>
                                                                <th>Asset Sub Type</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("AssetTypename")%></td>
                                                        <td><%# Eval("AssetSubTypename")%></td>
                                                        <td>
                                                           
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" CommandArgument="Delete" OnCommand="btnDeleteGrid_Command" CommandName='<%# Eval("AssetSubTypeCode")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this details?');"> </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

