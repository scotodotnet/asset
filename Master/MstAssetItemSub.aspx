﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstAssetItemSub.aspx.cs" Inherits="Master_MstAssetItemSub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Asset Item</h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Status</label>
                                            <asp:RadioButtonList ID="RblStatus" class="form-control" runat="server"
                                                RepeatColumns="4">
                                                <asp:ListItem Value="1" Text="Active" style="padding-right: 15px" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="IN Active"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Asset Type</label>
                                                <asp:DropDownList ID="ddlAssetType" runat="server" CssClass="form-control select2" AutoPostBack="true" OnTextChanged="ddlAssetType_TextChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAssetType" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Asset Sub Type</label>
                                                <asp:DropDownList ID="ddlAssetSubType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAssetSubType" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                  
                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label>Item Code</label>
                                                <asp:TextBox runat="server" ID="txtItemCode" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtItemCode" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Item Name</label>
                                                <asp:TextBox runat="server" ID="txtItemName" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtItemName" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

