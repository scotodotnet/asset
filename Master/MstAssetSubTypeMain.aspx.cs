﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Master_MstAssetSubTypeMain : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Load_Data();
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Master/MstAssetSubTypeSub.aspx");
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {

    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {

    }
}