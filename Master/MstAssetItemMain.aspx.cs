﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Master_MstAssetItem : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        Load_Data();
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "Select * from MstAssetItem where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        Session["AssetItemCode"] = e.CommandArgument.ToString();
        Session["AssetTypeCode"] = e.CommandName.Split(',').First().ToString();
        Session["AssetSubTypeCode"] = e.CommandName.Split(',').Last().ToString();
        Response.Redirect("/Master/MstAssetItemSub.aspx");
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Update MstAssetItem set Status='Delete' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and ItemCode='" + e.CommandArgument.ToString() + "' and AssetTypeCode='"+e.CommandName.Split(',').First().ToString()+"'";
        SSQL = SSQL + " and AssetSubTypeCode='"+e.CommandName.Split(',').Last().ToString()+"'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session.Remove("AssetItemCode");
        Session.Remove("AssetTypeCode");
        Session.Remove("AssetSubTypeCode");
        Response.Redirect("/Master/MstAssetItemSub.aspx");
    }
}