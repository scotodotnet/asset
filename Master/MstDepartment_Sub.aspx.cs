﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstDepartment_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearVal;
    string Dept_Code_Delete = "";
    string SessionDeptCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //SessionCcode = Session["Ccode"].ToString();
        //SessionLcode = Session["Lcode"].ToString();
        //SessionFinYearVal = Session["FinYear"].ToString();
        //SessionUserName = Session["Usernmdisplay"].ToString();
        //SessionUserID = Session["UserId"].ToString();

        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("../Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //if (!IsPostBack)
        //{
        //    Page.Title = "CORAL ERP :: Department";

        //    if (Session["DeptCode"] == null)
        //    {
        //        SessionDeptCode = "";
        //    }
        //    else
        //    {
        //        SessionDeptCode = Session["DeptCode"].ToString();
        //        txtDeptCode.Text = SessionDeptCode;
        //        //txtVouchNo.ReadOnly = true;
        //        btnSearch_Click(sender, e);
        //    }
            
        //}
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();
        //string SaveMode = "Insert";
        ////User Rights Check Start
        //bool ErrFlag = false;
        //bool Rights_Check = false;

        //GetIPAndName getIPAndName = new GetIPAndName();
        //string IP = getIPAndName.GetIP();
        //string HostName = getIPAndName.GetName();

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Department Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        ////Auto generate Transaction Function Call
        //if (btnSave.Text != "Update")
        //{
        //    if (!ErrFlag)
        //    {
        //        TransactionNoGenerate TransNO = new TransactionNoGenerate();
        //        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Department Master", SessionFinYearVal, "1");

        //        if (Auto_Transaction_No == "")
        //        {
        //            ErrFlag = true;
        //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
        //        }
        //        else
        //        {
        //            txtDeptCode.Text = Auto_Transaction_No;
        //        }
        //    }
        //}

        ////duplicate Check
        //if (btnSave.Text == "Save")
        //{
        //    SSQL = "Select * from MstDepartment Where DeptName='" + txtDeptName.Text + "' And ";
        //    SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' ";

        //    DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //    if (DT.Rows.Count > 0)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Department Already Exsit');", true);
        //    }
        //}

        

        //if (!ErrFlag)
        //{

        //    if (btnSave.Text == "Update")
        //    {
        //        //Update Command

        //        SSQL = "Update MstDepartment set DeptName='" + txtDeptName.Text + "' Where Ccode='" + SessionCcode + "' And ";
        //        SSQL = SSQL + " Lcode ='" + SessionLcode + "' And DeptCode='" + txtDeptCode.Text + "' ";

        //        objdata.RptEmployeeMultipleDetails(SSQL);

        //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Designation Details Updated Successfully');", true);
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Department Details Updated Successfully');", true);
        //    }
        //    else
        //    {
        //        //Insert Command
        //        SSQL = "Insert Into MstDepartment(Ccode,Lcode,DeptCode,DeptName,Status,UserID,UserName,CreateOn,Host_IP,Host_Name) Values( ";
        //        SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + txtDeptCode.Text + "','" + txtDeptName.Text + "',";
        //        SSQL = SSQL + " 'Add','" + SessionUserID + "','" + SessionUserName + "',Getdate(),'"+ IP +"',";
        //        SSQL = SSQL + " '" + HostName + "')";
        //        objdata.RptEmployeeMultipleDetails(SSQL);

        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('UOM Details Saved Successfully');", true);
        //        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
        //    }

        //    Clear_All_Field();
        //    btnSave.Text = "Save";
        //    txtDeptCode.Enabled = true;

        //    Response.Redirect("MstDepartment_Main.aspx");

        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('UOM Details Already Saved');", true);
        //}
    }
    private void Clear_All_Field()
    {
        //txtDeptCode.ReadOnly = false;
        txtDeptCode.Text = "";
        txtDeptName.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();

        //SSQL = "Select * from MstDepartment Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " Status <> 'Delete' And DeptCode='" + txtDeptCode.Text + "'";

        //DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (DT.Rows.Count != 0)
        //{
        //    //txtDeptCode.ReadOnly = true;
        //    txtDeptName.Text = DT.Rows[0]["DeptName"].ToString();
        //    btnSave.Text = "Update";
        //}
        //else
        //{
        //    Clear_All_Field();
        //}
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstDepartment_Main.aspx");
    }
}
