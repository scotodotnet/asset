﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Master_MstAssetTypeSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        if (!IsPostBack)
        {
            initial_DataRefresh();

            if ((string)Session["AssetTypeCode"] != null)
            {
                btnSearch(sender, e);
            }
        }
    }

    private void btnSearch(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstAssetType where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete' and ActiveStatus='1'";
        SSQL = SSQL + " and AssetTypeCode='" + Session["AssetTypeCode"].ToString() + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            txtAssettypeCode.Text = dt.Rows[0]["AssetTypeCode"].ToString();
            txtAssettypeName.Text = dt.Rows[0]["AssetTypeName"].ToString();


            SSQL = "";
            SSQL = "Select * from MstAssetSubType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AssetTypeCode='" + Session["AssetTypeCode"].ToString() + "'";
            DataTable dt_sub = new DataTable();
            dt_sub = objdata.RptEmployeeMultipleDetails(SSQL);
            btnSave.Text = "Update";
            ViewState["dt_Data"] = dt_sub;
            Load_Data();
        }                                                                  
    }

    private void initial_DataRefresh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("AssetTypeCode");
        dt.Columns.Add("AssetTypeName");
        dt.Columns.Add("AssetSubTypeCode");
        dt.Columns.Add("AssetSubTypeName");
        ViewState["dt_Data"] = dt;
        Load_Data();
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)(ViewState["dt_Data"]);
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        GetIPAndName getIPAndName = new GetIPAndName();
        //string _IP = getIPAndName.GetIP();
        string _IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (string.IsNullOrEmpty(_IP))
        {
            _IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        //string _HostName = getIPAndName.GetName();
        string _HostName = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];

        DataTable dt = new DataTable();
        dt = (DataTable)(ViewState["dt_Data"]);
        if (txtAssettypeName.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the AssetType Name')", true);
            return;
        }
        if (txtAssettypeCode.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the AssetType Code')", true);
            return;
        }
        if (dt.Rows.Count <= 0)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Add Atleast One Asset Sub Type')", true);
            return;
        }
        if (!ErrFlg)
        {
            if (btnSave.Text == "Update")
            {     
                SSQL = "";
                SSQL = "Select * from MstAssetType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                SSQL = SSQL + " AssetTypeName='" + txtAssettypeName.Text + "' and  Status!='Delete'";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_Check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The AssetTypename is Already Present')", true);
                    return;
                }

                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "update MstAssetType set AssetTypeName='" + txtAssettypeName.Text + "' where AssetTypeCode='" + txtAssettypeCode.Text + "' and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
                    objdata.RptEmployeeMultipleDetails(SSQL);   
                }
            }
            if (btnSave.Text == "Save")
            {    
                SSQL = "";
                SSQL = "Select * from MstAssetType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                SSQL = SSQL + " (AssetTypeCode ='" + txtAssettypeCode.Text + "' or AssetTypeName='" + txtAssettypeName.Text + "') and  Status!='Delete'";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_Check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Asset TypeCode or Asset Typename is Already Present')", true);
                    return;
                }

                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "Insert into MstAssetType (Ccode,Lcode,AssetTypeCode,AssetTypeName,CreatedOn,Host_IP,Host_Name,UserName,UserRole,Status,ActiveStatus)";
                    SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtAssettypeCode.Text + "','" + txtAssettypeName.Text + "',Convert(varchar,getdate(),105),";
                    SSQL = SSQL + " '" + _IP + "','" + _HostName + "','" + Session["UserId"] + "','" + Session["Isadmin"] + "','Add','1')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }  
            }

            if (dt.Rows.Count > 0)
            {
                SSQL = "";
                SSQL = "Delete from MstAssetSubType where ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AssettypeCode='" + txtAssettypeCode.Text + "' and  Status!='Delete'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                for (int i = 0; i < dt.Rows.Count; i++)
                {     
                    SSQL = "";
                    SSQL = "Insert into MstAssetSubType (CCode,Lcode,AssetTypeCode,AssetTypeName,CreatedOn,Host_IP,Host_Name,UserName,UserRole,AssetSubtypeCode,AssetSubTypeName,Status,ActiveStatus)";
                    SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtAssettypeCode.Text + "','" + txtAssettypeName.Text + "',convert(varchar,getdate(),105),'" + _IP + "',";
                    SSQL = SSQL + "'" + _HostName + "','" + Session["UserId"] + "','" + Session["Isadmin"] + "','" + dt.Rows[i]["AssetSubtypeCode"].ToString() + "','" + dt.Rows[i]["AssetSubTypeName"].ToString() + "',";
                    SSQL = SSQL + "'Add','1')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            if (!ErrFlg)
            {
                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset type Saved Successfully')", true);
                    btnClear_Click(sender, e);
                }
                else if(btnSave.Text=="Update")
                {                                                                                                                    
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset type Updated Successfully')", true);
                    btnClear_Click(sender, e);
                }
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

        initial_DataRefresh();
        txtAssetSubTypeName.Text = "";
        txtAssettypeCode.Text = "";
        txtAssettypeName.Text = "";
        txtSubTypeCode.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("AssetTypeCode");
        Response.Redirect("/Master/MstAssetTypeMain.aspx");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (txtAssetSubTypeName.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select AssetSubTypeName')", true);
            return;
        }
        if (txtSubTypeCode.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select Asset Sub Type Code')", true);
            return;
        }
        if (txtAssettypeName.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the AssetType Name')", true);
            return;
        }
        if (txtAssettypeCode.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the AssetType Code')", true);
            return;
        }
        DataTable dt_RowAdd = new DataTable();
        dt_RowAdd = (DataTable)(ViewState["dt_Data"]);
        DataRow dr;

        if (!ErrFlg)
        {
            if (dt_RowAdd.Rows.Count > 0)
            {
                SSQL = "";
                SSQL = "Select * from MstAssetSubType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and (AssetTypeCode='" + txtAssettypeCode.Text + "' and AssetSubtypeCode='" + txtSubTypeCode + "') or (AssetSubTypeName='" + txtAssetSubTypeName.Text + "' and AssetTypeName='" + txtAssettypeName.Text + "') and Status!='Delete'";
                DataTable dt_check = new DataTable();
                dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Item Already Present...')", true);
                    return;
                }
                if (!ErrFlg)
                {
                    for (int i = 0; i < dt_RowAdd.Rows.Count; i++)
                    {
                        if (dt_RowAdd.Rows[i]["AssetTypeCode"].ToString() == txtAssettypeCode.Text && dt_RowAdd.Rows[i]["AssetTypeName"].ToString() == txtAssettypeName.Text && dt_RowAdd.Rows[i]["AssetSubtypeCode"].ToString() == txtSubTypeCode.Text && dt_RowAdd.Rows[i]["AssetSubTypeName"].ToString() == txtAssetSubTypeName.Text)
                        {
                            ErrFlg = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Item Already Present...')", true);
                            break;
                        }
                    }
                }
                if (!ErrFlg)
                {
                    dr = dt_RowAdd.NewRow();
                    dr["AssetTypeCode"] = txtAssettypeCode.Text;
                    dr["AssetTypeName"] = txtAssettypeName.Text;
                    dr["AssetSubTypeCode"] = txtSubTypeCode.Text;
                    dr["AssetSubTypeName"] = txtAssetSubTypeName.Text;
                    dt_RowAdd.Rows.Add(dr);
                    txtAssetSubTypeName.Text = "";
                    txtSubTypeCode.Text = "";
                }
            }
            else
            {
                dr = dt_RowAdd.NewRow();
                dr["AssetTypeCode"] = txtAssettypeCode.Text;
                dr["AssetTypeName"] = txtAssettypeName.Text;
                dr["AssetSubTypeCode"] = txtSubTypeCode.Text;
                dr["AssetSubTypeName"] = txtAssetSubTypeName.Text;
                dt_RowAdd.Rows.Add(dr);
                txtAssetSubTypeName.Text = "";
                txtSubTypeCode.Text = "";
            }

            ViewState["dt_Data"] = dt_RowAdd;
            Load_Data();
        }
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        string AssetSubtypeName = e.CommandArgument.ToString();
        string AssetSubtypeCode= e.CommandName.ToString();

        txtAssetSubTypeName.Text = AssetSubtypeName.ToString();
        txtSubTypeCode.Text = AssetSubtypeCode.ToString();
        Load_Data();

    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();  
        dt = (DataTable)(ViewState["dt_Data"]);

        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = dt.Rows[i];

            if (dr["AssetSubTypeCode"].ToString() == e.CommandName.ToString())
                dr.Delete();
        }
        dt.AcceptChanges();
        ViewState["dt_Data"] = dt;
        Load_Data();
    }
}