﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Master_Bank : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        Load_Data_Bank();
    }
    private void Load_Data_Bank()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Bank where Status!='Delete'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Bank where BankName='" + e.CommandName.ToString() + "' And Branch='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtBankName.Text = DT.Rows[0]["BankName"].ToString();
            txtBranch.Text = DT.Rows[0]["Branch"].ToString();
            txtIFSCCode.Text = DT.Rows[0]["IFSCCode"].ToString();

            txtChargesAmt.Text = DT.Rows[0]["AmtCharge"].ToString();
            txtMinCharge.Text = DT.Rows[0]["MinCharge"].ToString();
            txtMaxCharge.Text = DT.Rows[0]["MaxCharge"].ToString();

            if (DT.Rows[0]["Default_Bank"].ToString() == "Yes")
            {
                chkDefault.Checked = true;
            }
            else
            {
                chkDefault.Checked = false;
            }

            btnSave.Text = "Update";
        }
        else
        {
            txtBankName.Text = "";
            txtBranch.Text = "";
            txtIFSCCode.Text = "";
            chkDefault.Checked = false;
        }

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Bank where BankName='" + e.CommandName.ToString() + "' And Branch='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Update  Bank set Status='Delete' where BankName='" + e.CommandName.ToString() + "' And Branch='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Bank Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Bank Not Found');", true);
        }
        Load_Data_Bank();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        string Def_Chk = "";
        bool ErrFlag = false;

        if (chkDefault.Checked == true)
        {
            Def_Chk = "Yes";
        }
        else
        {
            Def_Chk = "No";
        }

        if (Def_Chk == "Yes")
        {
            query = "select * from Bank where Default_Bank='Yes'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Already Default Bank exist.. Check it.!');", true);
            }
            else
            {
                ErrFlag = false;
            }
        }
        if (!ErrFlag)
        {
            GetIPAndName getIPAndName = new GetIPAndName();
            string _IP = getIPAndName.GetIP();
            string _HostName = getIPAndName.GetName();

            query = "select * from Bank where BankName='" + txtBankName.Text.ToUpper() + "' and IFSCCode='" + txtIFSCCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = "delete from Bank where BankName='" + txtBankName.Text.ToUpper() + "' and IFSCCode='" + txtIFSCCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Insert";
            }

            query = "Insert into Bank (BankName,Branch,IFSCCode,Default_Bank,AmtCharge,MinCharge,MaxCharge,CreatedOn,Host_IP,Host_Name,UserName,Status)";
            query = query + "values('" + txtBankName.Text.ToUpper() + "','" + txtBranch.Text.ToUpper() + "','" + txtIFSCCode.Text + "','" + Def_Chk + "','" + txtChargesAmt.Text + "','" + txtMinCharge.Text + "','" + txtMaxCharge.Text + "',";
            query = query + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + Session["UserID"].ToString() + "','Add')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Saved Successfully...!');", true);
            }
            Clear_All_Field();
        }
        Load_Data_Bank();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtBankName.Text = "";
        txtIFSCCode.Text = "";
        txtBranch.Text = "";
        chkDefault.Checked = false;
        btnSave.Text = "Save";
        txtChargesAmt.Text = "";
        txtMinCharge.Text = "";
        txtMaxCharge.Text = "";
    }
}
