﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Master_MstAssetItemSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        if (!IsPostBack)
        {
            Load_AssetType();
            if ((string)Session["AssetTypeCode"] != null)
            {
                btnSearch(sender, e);
            }
        }
    }

    private void Load_AssetType()
    {
        SSQL = "";
        SSQL = "Select * from MstAssetType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlAssetType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAssetType.DataTextField = "AssetTypeName";
        ddlAssetType.DataValueField = "AssetTypeCode";
        ddlAssetType.DataBind();
        ddlAssetType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void btnSearch(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstAssetItem where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete' and ActiveStatus='1'";
        SSQL = SSQL + " and AssetTypeCode='" + Session["AssetTypeCode"].ToString() + "' and ItemCode='" + Session["AssetItemCode"] + "' and AssetSubTypeCode='"+ Session["AssetSubTypeCode"] + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            ddlAssetType.SelectedValue = dt.Rows[0]["AssetTypeCode"].ToString();
            ddlAssetType_TextChanged(sender, e);
            ddlAssetSubType.SelectedValue = dt.Rows[0]["AssetSubTypeCode"].ToString();  

            txtItemCode.Enabled = false;
            txtItemCode.Text = dt.Rows[0]["ItemCode"].ToString();
            txtItemName.Text = dt.Rows[0]["ItemName"].ToString();
            txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            RblStatus.SelectedValue = dt.Rows[0]["ActiveStatus"].ToString();
            btnSave.Text = "Update" ;

        }
    }
      
    protected void btnSave_Click(object sender, EventArgs e)
    {
        GetIPAndName getIPAndName = new GetIPAndName();
        //string _IP = getIPAndName.GetIP();
        string _IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (string.IsNullOrEmpty(_IP))
        {
            _IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        //string _HostName = getIPAndName.GetName();
        string _HostName = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
                                                 
        if (ddlAssetSubType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the AssetType Name')", true);
            return;
        }
        if (ddlAssetSubType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the AssetSub Type')", true);
            return;
        }
        if (txtItemCode.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Item Code')", true);
            return;
        }
        if (txtItemName.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Item Name')", true);
            return;
        }
        if (RblStatus.SelectedValue == null)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Active Status')", true);
            return;
        }
        if (!ErrFlg)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Select * from MstAssetItem where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                SSQL = SSQL + " AssetTypeCode='" + ddlAssetType.SelectedItem.Value + "' and AssetSubTypeCode='"+ddlAssetSubType.SelectedValue+"' and  Status!='Delete'";
                SSQL = SSQL + " and ItemCode='" + txtItemCode.Text + "' and ItemName='" + txtItemName.Text + "'";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_Check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Item is Already Present')", true);
                    return;
                }

                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "update MstAssetItem set ItemName='" + txtItemName.Text + "', AssetTypeCode='" + ddlAssetType.SelectedValue + "',";
                    SSQL = SSQL + "AssetTypeName='" + ddlAssetType.SelectedItem.Text + "',AssetSubTypeCode='" + ddlAssetSubType.SelectedValue + "',AssetSubTypeName='" + ddlAssetSubType.SelectedItem.Text + "'";
                    SSQL = SSQL + " where itemCode='" + txtItemCode.Text + "' and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";  
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstAssetItem where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                SSQL = SSQL + " AssetTypeCode ='" + ddlAssetType.SelectedValue + "' and AssetSubTypeCode='" + ddlAssetSubType.SelectedValue + "' and  Status!='Delete'";
                SSQL = SSQL + " and (ItemCode='" + txtItemCode.Text + "' or ItemName='" + txtItemName.Text + "')";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_Check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The ItemCode or Item name is Already Present')", true);
                    return;
                }

                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "Insert into MstAssetItem (Ccode,Lcode,AssetTypeCode,AssetTypeName,CreatedOn,Host_IP,Host_Name,UserName,UserRole,Status,ActiveStatus,AssetSubTypeCode,AssetSubTypeName,ItemCode,ItemName,Remarks)";
                    SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlAssetType.SelectedValue + "','" + ddlAssetType.SelectedItem.Text + "',Convert(varchar,getdate(),105),";
                    SSQL = SSQL + " '" + _IP + "','" + _HostName + "','" + Session["UserId"] + "','" + Session["Isadmin"] + "','Add','1','" + ddlAssetSubType.SelectedValue + "','" + ddlAssetSubType.SelectedItem.Text + "','" + txtItemCode.Text + "','" + txtItemName.Text + "','" + txtRemarks.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }  
            if (!ErrFlg)
            {
                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset type Saved Successfully')", true);
                    btnClear_Click(sender, e);
                }
                else if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset type Updated Successfully')", true);
                    btnClear_Click(sender, e);
                }
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

        txtItemCode.Text = "";
        ddlAssetSubType.ClearSelection();
        ddlAssetType.ClearSelection();
        txtItemName.Text = "";
        RblStatus.ClearSelection();
        txtRemarks.Text = "";
        btnSave.Text = "Save";

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Master/MstAssetItemMain.aspx");
    }

    protected void ddlAssetType_TextChanged(object sender, EventArgs e)
    {
        Load_AssetSubType(ddlAssetType.SelectedValue);
    }

    private void Load_AssetSubType(string selectedValue)
    {
        SSQL = "";
        SSQL = "Select * from MstAssetSubType where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
        SSQL = SSQL + " and AssetTypeCode='" + selectedValue + "'";
        ddlAssetSubType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAssetSubType.DataTextField = "AssetSubTypeName";
        ddlAssetSubType.DataValueField = "AssetSubTypeCode";
        ddlAssetSubType.DataBind();
        ddlAssetSubType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
}