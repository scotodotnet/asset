﻿$(document).ready(function () {
    $(".DeptList").change(function () {

        var Dept = $(this).children("option:selected").text();

        if (Dept != "-Select-"){
            $.ajax({
                type: "POST",
                url: "/Trans_Pur_Ord_Main/MaterialListDeptWise?Dept=" + Dept,
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response.length > 0) {
                        $("#grid tbody").empty();
                        $.each(response, function (i, member) {
                            console.log(response[i]);
                            var rows = "<tr>"
                                + "<td hidden></td>"
                                + "<td class='customertd'>" + response[i].raw_Mat_Name + "</td>"
                                + "<td class='Qty' contenteditable></td>"
                                + "<td contenteditable>50</td>"
                                + "<td hidden class='customertd'>" + response[i].cgstp + "</td>"
                                + "<td></td>"
                                + "<td hidden class='customertd'>" + response[i].sgstp + "</td>"
                                + "<td></td>"
                                + "<td hidden class='customertd'>" + response[i].igstp + "</td>"
                                + "<td></td>"
                                + "<td class='Others' contenteditable></td>"
                                + "<td ></td>"
                                + "<td hidden></td>"
                                + "</tr>";
                            $('#grid tbody').append(rows);
                        });
                    }
                }
            });
        }
    });

    $("#grid").on("focusout", ".Qty", function () {
        var currentRow = $(this).closest("tr");
        var Qty = currentRow.find("td:eq(2)");
        var Rate = currentRow.find("td:eq(3)");
        var CGST = currentRow.find("td:eq(4)");
        var CGSTAmt = currentRow.find("td:eq(5)");
        var SGST = currentRow.find("td:eq(6)");
        var SGSTAmt = currentRow.find("td:eq(7)");
        var IGST = currentRow.find("td:eq(8)");
        var IGSTAmt = currentRow.find("td:eq(9)");
        var Amt = currentRow.find("td:eq(11)");

        if (Qty != "") {
            $.ajax({
                type: "POST",
                url: "/Trans_Pur_Ord_Main/AmtCalculation?Qty=" + Qty.text() + "&Rate=" + Rate.text() + "&CGST=" + CGST.text() + "&SGST=" + SGST.text() + "&IGST=" + IGST.text(),
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response.length > 0) {

                        Amt.text("");
                        CGSTAmt.text("");
                        SGSTAmt.text("");
                        IGSTAmt.text("");
                        CGSTAmt.text(response[0].cgstAmt);
                        SGSTAmt.text(response[0].sgstAmt);
                        IGSTAmt.text(response[0].igstAmt);
                        Amt.text(response[0].netAmt);

                        SumTotalAmt();
                        SumTotalTax();
                    }
                }
            });
        }
    });

    $("#grid").on("focusout", ".Others", function () {

        var currentRow = $(this).closest("tr");
        var Qty = currentRow.find("td:eq(2)");
        var Rate = currentRow.find("td:eq(3)");
        var CGST = currentRow.find("td:eq(4)");
        var SGST = currentRow.find("td:eq(6)");
        var IGST = currentRow.find("td:eq(8)");
        var Others = currentRow.find("td:eq(10)");
        var Amt = currentRow.find("td:eq(11)");

        if (Qty != "") {
            $.ajax({
                type: "POST",
                url: "/Trans_Pur_Ord_Main/OthersCalculation?Qty=" + Qty.text() + "&Rate=" + Rate.text() + "&CGST=" + CGST.text() + "&SGST=" + SGST.text() + "&IGST=" + IGST.text()+"&Others=" + Others.text(),
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response.length > 0) {
                        Amt.text("");
                        Amt.text(response[0].netAmt);
                        SumTotalAmt();
                        SumTotalOthers();
                    }
                }
            });
        }

    });


    $("#grid").on("keyup", ".Qty", function () {
        $("#grid").on(".Qty").each(function () {
            SumTotalQty();
            SumTotalAmt();
        });
    });

    function SumTotalQty() {
        var SumQty = 0;
        $(".Qty").each(function () {

            var currentRow = $(this).closest("tr");
            var Qty = currentRow.find("td:eq(2)");

            if (Qty.text() != 0) {
                SumQty += parseInt(Qty.text());
            }
        });

        var totQty = SumQty.toFixed(2);

        $(".txtTotQty").val(totQty);
    }

    function SumTotalAmt() {
        var SumNetAmt = 0;
        $(".Qty").each(function () {
            var currentRow = $(this).closest("tr");
            var NetAmt = currentRow.find("td:eq(11)");
            //alert(NetAmt.text());
            if (NetAmt.text() != 0) {
                //alert("test");
                SumNetAmt += parseFloat(NetAmt.text());
            }
        });
        var totNetAmt = SumNetAmt.toFixed(2);

        $(".totLineAmt").val(totNetAmt);
        $(".totNetAmt").val(totNetAmt);
    }

    function SumTotalOthers() {
        var SumOthers = 0;
        $(".Others").each(function () {
            var currentRow = $(this).closest("tr");
            var Others = currentRow.find("td:eq(10)");

            if (Others.text() != 0) {
                SumOthers += parseInt(Others.text());
            }
        });

        var totOthers = SumOthers.toFixed(2);

        $(".txtTotOther").val(totOthers);
    }
    
    function SumTotalTax() {

        var SumCGSTAmt = 0;
        var SumSGSTAmt = 0;
        var SumIGSTAmt = 0;
        $(".Qty").each(function () {
            var currentRow = $(this).closest("tr");
            var CGSTAmt = currentRow.find("td:eq(5)");
            var SGSTAmt = currentRow.find("td:eq(7)");
            var IGSTAmt = currentRow.find("td:eq(9)");

            if (CGSTAmt.text() != 0 || IGSTAmt.text() != 0) {
                SumCGSTAmt += parseFloat(CGSTAmt.text());
                SumSGSTAmt += parseFloat(SGSTAmt.text());
                SumIGSTAmt += parseFloat(IGSTAmt.text());
            }
        });

        var totCGSTAmt = SumCGSTAmt.toFixed(2);
        var totSGSTAmt = SumSGSTAmt.toFixed(2);
        var totIGSTAmt = SumIGSTAmt.toFixed(2);

        $(".txtTotCGST").val(totCGSTAmt);
        $(".txtTotSGST").val(totSGSTAmt);
        $(".txtTotIGST").val(totIGSTAmt);
    }

    $(".RoundOff").focusout(function () {
        var RoundOff = $(this).val();
        var totLineAmt = $(".totLineAmt").val();
        if (RoundOff != "") {
            $.ajax({
                type: "POST",
                url: "/Trans_Pur_Ord_Main/RoundOffCalculation?RoundOff=" + RoundOff + "&totLineAmt=" + totLineAmt,
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response.length > 0) {
                        //alert(response[0].netAmt);
                        //Amt.text("");
                        //Amt.text(response[0].netAmt);
                        $(".totNetAmt").val(response[0].netAmt);
                    }
                }
            });
        }
    });

    $(".TranMain").click(function () {
        alert("Main Form Cal");
        //$(".TranSub").trigger();
    })
});
//function Subfrom(form) {
//    alert("Sub Form Cal");
//}