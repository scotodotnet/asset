﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AssetIssueSub.aspx.cs" Inherits="Transaction_AssetItemSub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Asset Item Issue/Return</h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Issue Status</label>
                                            <asp:RadioButtonList ID="RblStatus" class="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RblStatus_SelectedIndexChanged"
                                                RepeatColumns="4">
                                                <asp:ListItem Value="1" Text="Item Return" style="padding-right: 15px"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Item Issue" style="padding-right: 15px"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                               <label>TransCode</label>
                                               <asp:Label runat="server" ID="txtTransCode" Enabled="false" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Asset Type</label>
                                                <asp:DropDownList ID="ddlAssetType" runat="server" CssClass="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlAssetType_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAssetType" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Asset Sub Type</label>
                                                <asp:DropDownList ID="ddlAssetSubtype" runat="server" CssClass="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlAssetSubtype_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAssetSubtype" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label>Item Code</label>
                                                <asp:TextBox runat="server" ID="txtItemCode" Enabled="false" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtItemCode" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Item Name</label>
                                                <asp:DropDownList ID="ddlItemName" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlItemName" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Barcode</label>
                                                <asp:TextBox runat="server" ID="txtBarcode" Enabled="false" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtbarcode" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Purchase Date</label>
                                                <asp:TextBox runat="server" ID="txtPurchaseDate" Enabled="false" class="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtPurchaseDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Manufacture Name</label>
                                                <asp:TextBox ID="txtManufacture" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtManufacture" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Date of Manufacturing</label>
                                                <asp:TextBox ID="txtManufacturingDate" runat="server" Enabled="false" CssClass="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtManufacturingDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Model</label>
                                                <asp:TextBox ID="txtModel" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtModel" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>


                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Serial No</label>
                                                <asp:TextBox ID="txtSerialno" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtSerialno" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Condition</label>
                                                <asp:DropDownList ID="ddlCondition" runat="server" CssClass="form-control select2">
                                                    <asp:ListItem Value="0" Text="New" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Refursable"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Damaged"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Other"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlCondition" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Warrenty</label>
                                                <asp:TextBox ID="txtWarrenty" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Qty</label>
                                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Stock Qty</label>
                                                <asp:TextBox ID="txtStockQty" runat="server" Enabled="false" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Return Description</label>
                                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="box-header with-border">
                                            </div>

                                            <div class="clearfix"></div>
                                            <br />

                                            <div class="box-body no-padding">
                                                <div class="row">
                                                     <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Checked IN By</label>
                                                            <asp:DropDownList ID="ddlCheckedINBy" runat="server" CssClass="form-control select2">
                                                                <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ControlToValidate="ddlCheckedINBy" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                                class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true"
                                                                ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                     <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Checked OUT By</label>
                                                            <asp:DropDownList ID="ddlCheckedOUTBy" runat="server" CssClass="form-control select2">
                                                                 <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ControlToValidate="ddlCheckedOUTBy" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                                class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true"
                                                                ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Checked IN</label>
                                                            <asp:DropDownList ID="ddlCheckedIN" runat="server" CssClass="form-control select2">
                                                                 <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ControlToValidate="ddlCheckedOUT" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                                class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true"
                                                                ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Checked OUT</label>
                                                            <asp:DropDownList ID="ddlCheckedOUT" runat="server" CssClass="form-control select2">
                                                                 <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ControlToValidate="ddlCheckedOUT" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                                class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true"
                                                                ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

