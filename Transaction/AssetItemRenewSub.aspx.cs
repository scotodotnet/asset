﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_AssetItemRenewSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        if (!IsPostBack)
        {
            Load_AssetType();         
            if ((string)Session["TransID"] != null)
            {
                btnSearch(sender, e);
            }
        }
    }
    private void Load_AssetType()
    {
        SSQL = "";
        SSQL = "Select * from MstAssetType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlAssetType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAssetType.DataTextField = "AssetTypeName";
        ddlAssetType.DataValueField = "AssetTypeCode";
        ddlAssetType.DataBind();
        ddlAssetType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    private void btnSearch(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from AssetItem_Renew_Service where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
        SSQL = SSQL + " and TransID='" + Session["TransID"].ToString() + "'";
        //SSQL = SSQL + " and AssetTypeCode='" + Session["AssetTypeCode"].ToString() + "' and ItemCode='" + Session["AssetItemCode"] + "' and AssetSubTypeCode='" + Session["AssetSubTypeCode"] + "' and InventoryLocationCode='" + Session["InventoryLocationCode"] + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            ddlAssetType.SelectedValue = dt.Rows[0]["AssetTypeCode"].ToString();
            ddlAssetType_SelectedIndexChanged(sender, e);
            ddlAssetSubtype.SelectedValue = dt.Rows[0]["AssetSubTypeCode"].ToString();
            ddlAssetSubtype_SelectedIndexChanged(sender, e);

            txtTransCode.Text = dt.Rows[0]["TransID"].ToString();

            txtItemCode.Text = dt.Rows[0]["ItemCode"].ToString();
            ddlItemName.SelectedValue = dt.Rows[0]["ItemCode"].ToString();

            RblStatus.Enabled = false;
            RblStatus.SelectedValue = dt.Rows[0]["IssueStatus"].ToString();     
            txtPurchaseDate.Text = dt.Rows[0]["PurchaseDate"].ToString();
            txtManufacture.Text = dt.Rows[0]["ManufactureName"].ToString();
            txtManufacturingDate.Text = dt.Rows[0]["ManufacturingDate"].ToString();
            txtModel.Text = dt.Rows[0]["Model"].ToString();
            txtSerialno.Text = dt.Rows[0]["SerialNo"].ToString();
            ddlCondition.SelectedValue = dt.Rows[0]["Condition"].ToString();
            txtWarrenty.Text = dt.Rows[0]["Warrenty"].ToString();

            ddlItemName_SelectedIndexChanged(sender, e);

            if (RblStatus.SelectedItem.Value == "1")
            {
                txtQty.Text = dt.Rows[0]["ReturnQty"].ToString();
              

            }
            else if (RblStatus.SelectedValue == "2")
            {
                txtQty.Text = dt.Rows[0]["IsseuQty"].ToString();
                
            }
            btnSave.Text = "Update";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        GetIPAndName getIPAndName = new GetIPAndName();
        //string _IP = getIPAndName.GetIP();
        string _IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (string.IsNullOrEmpty(_IP))
        {
            _IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        //string _HostName = getIPAndName.GetName();
        string _HostName = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];

        if (RblStatus.SelectedItem.Equals(null))
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Issue Status First')", true);
            return;
        }
        if (ddlAssetType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the AssetType Name')", true);
            return;
        }
        if (ddlAssetSubtype.Items.Count == 0 || ddlAssetSubtype.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the AssetType Code')", true);
            return;
        }
        if (ddlItemName.Items.Count == 0 || ddlItemName.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Item Name')", true);
            return;
        }
        if (txtQty.Text == "0" || txtQty.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Quantity')", true);
            return;
        }

        DataTable dt_toolkit = new DataTable();
        dt_toolkit = (DataTable)ViewState["dt_Data_Toolkit"];

        DataTable dt_SpareParts = new DataTable();
        dt_SpareParts = (DataTable)ViewState["dt_Data_SpareParts"];

        if (!ErrFlg)
        {

            decimal stock_Qty = 0;
            Check_Stock();


            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "update AssetItem_Renew_Service set AssetTypeName='" + ddlAssetType.SelectedItem.Text + "',AssetTypeCode='" + ddlAssetType.SelectedItem.Value + "'";
                SSQL = SSQL + ",AssetSubTypeCode='" + ddlAssetSubtype.SelectedValue + "',AssetSubTypeName='" + ddlAssetSubtype.SelectedItem.Text + "',";
                SSQL = SSQL + " ItemCode='" + ddlItemName.SelectedValue + "',ItemName='" + ddlItemName.SelectedItem.Text + "'";
                //SSQL = SSQL + ",InventoryLocationCode='" + ddlInventoryLocation.SelectedValue + "' , InventoryLocationName='" + ddlInventoryLocation.SelectedItem.Text + "'";
                SSQL = SSQL + ",PurchaseDate='" + txtPurchaseDate.Text + "',ManufactureName='" + txtManufacture.Text + "'";
                SSQL = SSQL + ",ManufacturingDate='" + txtManufacturingDate.Text + "',Model='" + txtModel.Text + "',SerialNo='" + txtSerialno.Text + "'";
                SSQL = SSQL + ",Warrenty='" + txtWarrenty.Text + "'";                                                                     

                if (RblStatus.SelectedValue == "2")
                {
                    SSQL = SSQL + ",Qty='" + txtQty.Text + "',Condition='" + ddlCondition.SelectedValue + "',IssueDate='" + txtServiceDate.Text + "',Reson='" + txtReson.Text + "'";

                }
                else if (RblStatus.SelectedValue == "1")
                {
                    SSQL = SSQL + ",Qty='" + txtQty.Text + "',ReceiveDate='" + txtDateofRecive.Text + "',";
                    SSQL = SSQL + " Cost='" + txtCostofRenewService.Text + "',Tax='" + txtTax.Text + "',NetAmt='" + txtRenewServiceNet.Text + "',";
                    SSQL = SSQL + ",Nextdate='"+txtNextServiceRenewDate.Text+"'";
                }

                SSQL = SSQL + ",IssueStatus='" + RblStatus.SelectedValue + "'";

                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
                SSQL = SSQL + " and ItemCode='" + txtItemCode.Text + "' and ItemName='" + ddlItemName.SelectedItem.Text + "' and TransID='"+Session["TransID"]+"'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "Insert Into AssetItem_Renew_Service (Ccode,Lcode,TransDate,AssetTypeCode,AssetTypeName,AssetSubTypeCode,AssetSubTypeName,ItemCode,ItemName,PurchaseDate,ManufactureName,ManufacturingDate,";
                    SSQL = SSQL + "Model,SerialNo,StockQty,Qty,Reson,IssueDate,Codition,ReceiveDate,Cost,Tax,NetAmt,Nextdate,Warrenty,IssueStatus,";
                    SSQL = SSQL + "CreatedOn,Host_IP,Host_Name,UserName,UserRole,Status) values(";
                    SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "',Convert(varchar,getdate(),105),'" + ddlAssetType.SelectedValue + "','" + ddlAssetType.SelectedItem.Text + "','" + ddlAssetSubtype.SelectedValue + "'";
                    SSQL = SSQL + ",'" + ddlAssetSubtype.SelectedItem.Text + "','" + txtItemCode.Text + "','" + ddlItemName.SelectedItem.Text + "','" + txtPurchaseDate.Text + "','" + txtManufacture.Text + "'";
                    SSQL = SSQL + ",'" + txtManufacturingDate.Text + "','" + txtModel.Text + "','" + txtSerialno.Text + "','" + txtStockQty.Text + "'";
                    if (RblStatus.SelectedValue == "2")
                    {
                        SSQL = SSQL + ",'" + txtQty.Text + "','" + txtReson.Text + "',Conver(date,'" + txtServiceDate.Text + "',103),'" + ddlCondition.SelectedItem.Text + "'";
                        SSQL = SSQL + ",'','0','0','0','',";
                       
                    }
                    else if (RblStatus.SelectedValue == "1")
                    {
                        SSQL = SSQL + ",'" + txtQty.Text + "','','',''";
                        SSQL = SSQL + ",Convert(date,'" + txtDateofRecive.Text + "',103),'" + txtCostofRenewService.Text + "','" + txtTax.Text + "','" + txtRenewServiceNet.Text + "',Convert(date,'" + txtNextServiceRenewDate.Text + "',103),";
                    }
                    SSQL = SSQL + "'" + txtWarrenty.Text + "','" + RblStatus.SelectedValue + "'";
                    SSQL = SSQL + " ,Convert(varchar,getdate(),105),'" + _IP + "','" + _HostName + "','" + Session["UserId"] + "','" + Session["Isadmin"] + "','Add')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            if (!ErrFlg)
            {
                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset Transaction Saved Successfully')", true);
                    btnClear_Click(sender, e);
                }
                else if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset Inventory Updated Successfully')", true);
                    btnClear_Click(sender, e);
                }
            }
        }
    }
    private void Check_Stock()
    {
        decimal stk_Qty = 0;
        SSQL = "";
        SSQL = "select sum(Qty) as StockQty from MstAssetItemInventory where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and AssetTypeCode='" + ddlAssetType.SelectedValue + "'";
        SSQL = SSQL + " and AssetSubTypeCode='" + ddlAssetSubtype.SelectedValue + "' and ItemCode='" + txtItemCode.Text + "'  and ActiveStatus='1'";
        //SSQL = SSQL + "and status='Aproved'";
        DataTable dt_Stock = new DataTable();
        dt_Stock = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_Stock.Rows.Count > 0)
        {
            stk_Qty = Convert.ToDecimal(dt_Stock.Rows[0]["StockQty"].ToString());
        }
        if (RblStatus.SelectedValue == "2")
        {
            stk_Qty = (stk_Qty - Convert.ToDecimal(txtQty.Text));
        }
        else if (RblStatus.SelectedValue == "1")
        {
            stk_Qty = (stk_Qty + Convert.ToDecimal(txtQty.Text));
        }
        txtStockQty.Text = stk_Qty.ToString();
        //SSQL = "";
        //SSQL = "Update MstAssetItemInventory set Qty='" + stk_Qty + "' where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and AssetTypeCode='" + ddlAssetType.SelectedValue + "'";
        //SSQL = SSQL + " and AssetSubTypeCode='" + ddlAssetSubtype.SelectedValue + "' and ItemCode='" + txtItemCode.Text + "' and status='Aproved' and ActiveStatus='1'";
        //objdata.RptEmployeeMultipleDetails(SSQL);
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        RblStatus.ClearSelection();
        txtTransCode.Text = "";
        ddlAssetType.ClearSelection();
        ddlAssetSubtype.ClearSelection();
        txtItemCode.Text = "";
        ddlItemName.ClearSelection();
        txtPurchaseDate.Text = "";
        txtManufacture.Text = "";
        txtManufacturingDate.Text = "";
        txtModel.Text = "";
        txtSerialno.Text = "";
        txtServiceDate.Text = "";
        ddlCondition.ClearSelection();
        txtReson.Text = "";
        txtDateofRecive.Text = "";
        txtCostofRenewService.Text = "0";
        txtTax.Text = "0";
        txtRenewServiceNet.Text = "0";
        txtNextServiceRenewDate.Text = "";
        txtWarrenty.Text = "";
        txtQty.Text = "0";
        txtStockQty.Text = "0";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Write("/Transaction/AssetItemRenewMain.aspx");
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtItemCode.Text = ddlItemName.SelectedValue;
        SSQL = "";
        SSQL = "Select * from MstAssetItemInventory where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete' and ActiveStatus='1'";
        SSQL = SSQL + " and AssetTypeCode='" + ddlAssetType.SelectedValue + "' and AssetSubTypeCode='" + ddlAssetSubtype.SelectedValue + "' and ItemCode='" + txtItemCode.Text + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {                                                       
            txtPurchaseDate.Text = dt.Rows[0]["PurchaseDate"].ToString();
            txtManufacture.Text = dt.Rows[0]["ManufactureName"].ToString();
            txtManufacturingDate.Text = dt.Rows[0]["ManufacturingDate"].ToString();
            txtModel.Text = dt.Rows[0]["Model"].ToString();
            txtSerialno.Text = dt.Rows[0]["SerialNo"].ToString();
            //ddlCondition.SelectedValue = dt.Rows[0]["Condition"].ToString();
            txtWarrenty.Text = dt.Rows[0]["Warrenty"].ToString();

            txtStockQty.Text = dt.Rows[0]["Qty"].ToString();
        }
        else
        {                             
            txtPurchaseDate.Text = "";
            txtManufacture.Text = "";
            txtManufacturingDate.Text = "";
            txtModel.Text = "";
            txtSerialno.Text = "";
           // ddlCondition.ClearSelection();
            txtWarrenty.Text = "";

            txtStockQty.Text = "0";
        }
    }

    protected void ddlAssetType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_AssetSubType(ddlAssetType.SelectedValue);
    }

    protected void ddlAssetSubtype_SelectedIndexChanged(object sender, EventArgs e)
    {

        SSQL = "";
        SSQL = "Select * from MstAssetItem where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete' and ActiveStatus='1'";
        SSQL = SSQL + " and AssetTypeCode='" + ddlAssetType.SelectedValue + "' and AssetSubTypeCode='" + ddlAssetSubtype.SelectedValue + "'";
        ddlItemName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "itemCode";
        ddlItemName.DataBind();
        ddlItemName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void RblStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RblStatus.SelectedValue == "1")
        {
            txtServiceDate.Enabled = false;
            ddlCondition.Enabled = false;
            txtDateofRecive.Enabled = true;
            txtCostofRenewService.Enabled = true;
            txtTax.Enabled = true;
            txtRenewServiceNet.Enabled = true;
            txtNextServiceRenewDate.Enabled = true;
        }
        else
        {
            txtServiceDate.Enabled = true;
            ddlCondition.Enabled = true;
            txtDateofRecive.Enabled = false;
            txtCostofRenewService.Enabled = false;
            txtTax.Enabled = false;
            txtRenewServiceNet.Enabled = false;
            txtNextServiceRenewDate.Enabled = false;
        }
    }
    private void Load_AssetSubType(string selectedValue)
    {
        SSQL = "";
        SSQL = "Select * from MstAssetSubType where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
        SSQL = SSQL + " and AssetTypeCode='" + selectedValue + "'";
        ddlAssetSubtype.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAssetSubtype.DataTextField = "AssetSubTypeName";
        ddlAssetSubtype.DataValueField = "AssetSubTypeCode";
        ddlAssetSubtype.DataBind();
        ddlAssetSubtype.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
}