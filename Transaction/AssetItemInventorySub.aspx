﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AssetItemInventorySub.aspx.cs" Inherits="Transaction_AssetItemInventorySub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Asset InVentory</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Status</label>
                                            <asp:RadioButtonList ID="RblStatus" class="form-control" runat="server"
                                                RepeatColumns="4">
                                                <asp:ListItem Value="1" Text="Active" style="padding-right: 15px" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="IN Active"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Asset Type</label>
                                                <asp:DropDownList ID="ddlAssetType" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlAssetType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAssetType" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Asset Sub Type</label>
                                                <asp:DropDownList ID="ddlAssetSubType" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlAssetSubType_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAssetSubType" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Inventory Location</label>
                                                <asp:DropDownList ID="ddlInventoryLocation" runat="server" CssClass="form-control select2"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlInventoryLocation" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Photo</label>
                                                <br />
                                                <img src="/assets/images/NoImage.png" style="width: 110px; height: 60px; cursor: pointer;" class="img-thumbnail" id="MaterialImg" />
                                                <input style="visibility: hidden" type="file" name="Img" class="form-control" onchange='sendFile(this);' id="f_UploadImage" />
                                                <asp:HiddenField ID="hidd_imgPath" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label>Item Code</label>
                                                <asp:TextBox runat="server" ID="txtItemCode" class="form-control" Enabled="false"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtItemCode" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Item Name</label>
                                                <asp:DropDownList ID="ddlItemName" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlItemName" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Barcode</label>
                                                <asp:TextBox runat="server" ID="txtBarcode" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtbarcode" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Purchase Date</label>
                                                <asp:TextBox runat="server" ID="txtPurchaseDate" class="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtPurchaseDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Manufacture Name</label>
                                                <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtManufacture" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Date of Manufacturing</label>
                                                <asp:TextBox ID="txtManufacturingDate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtManufacturingDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Model</label>
                                                <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtModel" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Serial No</label>
                                                <asp:TextBox ID="txtSerialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtSerialno" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Working Condition</label>
                                                <asp:DropDownList ID="ddlCondition" runat="server" CssClass="form-control select2">
                                                    <asp:ListItem Value="New" Text="New" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="Refursable" Text="Refursable"></asp:ListItem>
                                                    <asp:ListItem Value="Damaged" Text="Damaged"></asp:ListItem>
                                                    <asp:ListItem Value="Other" Text="Other"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlCondition" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Warrenty</label>
                                                <asp:TextBox ID="txtWarrenty" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Qty</label>
                                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtQty_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Value</label>
                                                <asp:TextBox ID="txtValue" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtValue_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Amonut</label>
                                                <asp:TextBox ID="txtAmount"  runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="box box-warning">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Tool kit Item Details <i>(Opt.)</i></h3>
                                                </div>

                                                <div class="box-body no-padding">
                                                    <div class="row">
                                                         <div class="col-md-4">
                                                             <div class="form-group">
                                                                 <label for="exampleInputName">Tool Kit Status</label>
                                                                 <asp:RadioButtonList ID="rblToolkitStatus" class="form-control" runat="server"
                                                                     RepeatColumns="4">
                                                                     <asp:ListItem Value="1" Text="Active" style="padding-right: 15px" Selected="True"></asp:ListItem>
                                                                     <asp:ListItem Value="2" Text="IN Active"></asp:ListItem>
                                                                 </asp:RadioButtonList>
                                                             </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Tool Kit Name</label>
                                                                <asp:TextBox ID="txtToolKitName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ControlToValidate="txtToolKitName" Display="Dynamic" ValidationGroup="Validate_ToolkitField"
                                                                    class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true"
                                                                    ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Qty</label>
                                                                <asp:TextBox ID="txtToolkitQty" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtToolkitQty_TextChanged"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ControlToValidate="txtToolkitQty" Display="Dynamic" ValidationGroup="Validate_ToolkitField"
                                                                    class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true"
                                                                    ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Value</label>
                                                                <asp:TextBox ID="txtToolKitValue" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtToolKitValue_TextChanged"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ControlToValidate="txtToolKitValue" Display="Dynamic" ValidationGroup="Validate_ToolkitField"
                                                                    class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true"
                                                                    ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Amount</label>
                                                                <asp:TextBox ID="txtToolkitAmount" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtToolkitAmount_TextChanged"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ControlToValidate="txtToolkitAmount" Display="Dynamic" ValidationGroup="Validate_ToolkitField"
                                                                    class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true"
                                                                    ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <br />
                                                                <asp:Button ID="btnAddtoolkit" class="btn btn-success" runat="server" Text="Add" ValidationGroup="Validate_ToolkitField" OnClick="btnAddtoolkit_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-body no-padding">
                                                    <div class="table-responsive mailbox-messages">
                                                        <div class="col-md-12">
                                                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S. No</th>
                                                                                <th>ToolkitName</th>
                                                                                <th>Qty</th>
                                                                                <th>Value</th>
                                                                                <th>Amount</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                                        <td><%# Eval("Toolkit_Name")%></td>
                                                                        <td><%# Eval("Qty")%></td>
                                                                        <td><%# Eval("Value")%></td>
                                                                        <td><%# Eval("Amount")%></td>
                                                                        
                                                                        <td>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" CommandArgument="Delete" OnCommand="btnToolkitDeleteGrid_Command" CommandName='<%#Eval("Toolkit_Name") %>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>

                                                        </div>
                                                        <!-- /.table -->
                                                    </div>
                                                    <!-- /.mail-box-messages -->
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6"></div>
                                                <div class="form-group col-md-2">

                                                    <label for="exampleInputName">Total Amt</label>
                                                    <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0" AutoPostBack="true" OnTextChanged="txtTotAmt_TextChanged"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>

                                                <div class="form-group col-md-1" runat="server" visible="false">
                                                    <label for="exampleInputName">Tax Per</label>
                                                    <asp:TextBox ID="txtTax" class="form-control" runat="server"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtTax" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <div class="form-group col-md-2" runat="server" visible="false">
                                                    <label for="exampleInputName">Tax Amount</label>
                                                    <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0"></asp:Label>
                                                </div>
                                                <div class="form-group col-md-1" runat="server" visible="false">
                                                    <label for="exampleInputName">Discount</label>
                                                    <asp:TextBox ID="txtDiscount" class="form-control" runat="server"
                                                        OnTextChanged="txtDiscount_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtDiscount" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <div class="form-group col-md-2" runat="server" visible="false">
                                                    <label for="exampleInputName">Add or Less</label>
                                                    <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server"
                                                        Text="0.0" AutoPostBack="true" OnTextChanged="txtAddOrLess_TextChanged"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Net Amt</label>
                                                    <asp:Label ID="txtNetAmt" runat="server" class="form-control" Text="0.0"></asp:Label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="box box-warning">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Spare Parts Item Details <i>(Opt.)</i></h3>
                                                </div>
                                                <div class="box-body no-padding">
                                                     <div class="row">
                                                         <div class="col-md-4">
                                                             <div class="form-group">
                                                                 <label for="exampleInputName">Spare Parts Status</label>
                                                                 <asp:RadioButtonList ID="rblSparePartsStatus" class="form-control" runat="server"
                                                                     RepeatColumns="4">
                                                                     <asp:ListItem Value="1" Text="Active" style="padding-right: 15px" Selected="True"></asp:ListItem>
                                                                     <asp:ListItem Value="2" Text="IN Active"></asp:ListItem>
                                                                 </asp:RadioButtonList>

                                                             </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Spare Parts Name</label>
                                                                <asp:TextBox ID="txtSparePartsName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ControlToValidate="txtSparePartsName" Display="Dynamic" ValidationGroup="Validate_SpareField"
                                                                    class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true"
                                                                    ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Qty</label>
                                                                <asp:TextBox ID="txtSpareQty" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtSpareQty_TextChanged"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ControlToValidate="txtSpareQty" Display="Dynamic" ValidationGroup="Validate_SpareField"
                                                                    class="form_error" ID="RequiredFieldValidator18" runat="server" EnableClientScript="true"
                                                                    ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Value</label>
                                                                <asp:TextBox ID="txtSpareValue" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtSpareValue_TextChanged"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ControlToValidate="txtSpareValue" Display="Dynamic" ValidationGroup="Validate_SpareField"
                                                                    class="form_error" ID="RequiredFieldValidator19" runat="server" EnableClientScript="true"
                                                                    ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Amount</label>
                                                                <asp:TextBox ID="txtSpareAmt" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtSpareAmt_TextChanged"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ControlToValidate="txtSpareAmt" Display="Dynamic" ValidationGroup="Validate_SpareField"
                                                                    class="form_error" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true"
                                                                    ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <br />
                                                                <asp:Button ID="btnSpareAdd" class="btn btn-success" runat="server" Text="Add" ValidationGroup="Validate_SpareField" OnClick="btnSpareAdd_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-body no-padding">
                                                    <div class="table-responsive mailbox-messages">
                                                        <div class="col-md-12">
                                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S. No</th>
                                                                                <th>SpareParts Name</th>
                                                                                <th>Qty</th>
                                                                                <th>Value</th>
                                                                                <th>Amount</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                                        <td><%# Eval("SparePartsName")%></td>
                                                                        <td><%# Eval("Qty")%></td>
                                                                        <td><%# Eval("Value")%></td>
                                                                        <td><%# Eval("Amount")%></td>
                                                                        
                                                                        <td>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" CommandArgument="Delete" OnCommand="btnSparePartsDeleteGrid_Command" CommandName='<%#Eval("SparePartsName")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                        <!-- /.table -->
                                                    </div>
                                                    <!-- /.mail-box-messages -->
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6"></div>
                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Total Amt</label>
                                                    <asp:TextBox ID="txtSpareTotalAmt" class="form-control" runat="server" Text="0.0" AutoPostBack="true" OnTextChanged="txtSpareTotalAmt_TextChanged"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtSpareTotalAmt" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <div class="form-group col-md-1" runat="server" visible="false">
                                                    <label for="exampleInputName">Tax Per</label>
                                                    <asp:TextBox ID="txtSpareTax" class="form-control" runat="server" ></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtSpareTax" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <div class="form-group col-md-2" runat="server" visible="false">
                                                    <label for="exampleInputName">Tax Amount</label>
                                                    <asp:Label ID="txtSpareTaxAmt" runat="server" class="form-control" Text="0"></asp:Label>
                                                </div>
                                                <div class="form-group col-md-1" runat="server" visible="false">
                                                    <label for="exampleInputName">Discount</label>
                                                    <asp:TextBox ID="txtSpareDiscount" class="form-control" runat="server"
                                                        OnTextChanged="txtSpareDiscount_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtSpareDiscount" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <div class="form-group col-md-2" runat="server" visible="false">
                                                    <label for="exampleInputName">Add or Less</label>
                                                    <asp:TextBox ID="txtSpareAmtAddLess" class="form-control" runat="server"
                                                        Text="0.0" AutoPostBack="true" OnTextChanged="txtSpareAmtAddLess_TextChanged"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtSpareAmtAddLess" ValidChars="0123456789.-">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Net Amt</label>
                                                    <asp:Label ID="txtSpareNetAmt" runat="server" class="form-control" Text="0.0"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" align="right">
                                        <div class="col-md-6">
                                            </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Total AssetAmt</label>
                                                <asp:TextBox ID="txtTotalAssetAmt" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtTotalAssetAmt_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>RoundOff</label>
                                                <asp:TextBox ID="txtRoundOff" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtRoundOff_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Net Amount</label>
                                                <asp:TextBox ID="txtTotalAssetnetAmt" runat="server" CssClass="form-control" Text="0.0" AutoPostBack="true" OnTextChanged="txtTotalAssetnetAmt_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/Master/BOMMasters.js"></script>
    <script src="assets/adminlte/plugins/iCheck/icheck.min.js"></script>
     <script>
        $(function () {
            $('.checkbox').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.select2').select2();
                }
                $(function () {
                    $('.checkbox').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%' /* optional */
                    });
                });
            });
        };
    </script>
</asp:Content>

