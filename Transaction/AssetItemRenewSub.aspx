﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AssetItemRenewSub.aspx.cs" Inherits="Transaction_AssetItemRenewSub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Asset Item Renew/Service Entry</h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                          <div class="form-group col-md-3">
                                            <label for="exampleInputName">Status</label>
                                            <asp:RadioButtonList ID="RblStatus" class="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RblStatus_SelectedIndexChanged"
                                                RepeatColumns="4">
                                               <asp:ListItem Value="1" Text="Item Return" style="padding-right: 15px"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Item Issue" style="padding-right: 15px"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>TransCode</label>
                                               <asp:Label runat="server" ID="txtTransCode" Enabled="false" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Asset Type</label>
                                                <asp:DropDownList ID="ddlAssetType" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlAssetType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAssetType" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Asset Sub Type</label>
                                                <asp:DropDownList ID="ddlAssetSubtype" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlAssetSubtype_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAssetSubtype" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label>Item Code</label>
                                                <asp:TextBox runat="server" ID="txtItemCode" class="form-control" Enabled="false"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtItemCode" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Item Name</label>
                                                <asp:DropDownList ID="ddlItemName" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlItemName" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        </div>
                                    <div class="row">
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Purchase Date</label>
                                                <asp:TextBox runat="server" ID="txtPurchaseDate" class="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtPurchaseDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                   
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Manufacture Name</label>
                                                <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtManufacture" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                  
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Date of Manufacturing</label>
                                                <asp:TextBox ID="txtManufacturingDate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtManufacturingDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                           <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Model</label>
                                                <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtModel" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                  
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Serial No</label>
                                                <asp:TextBox ID="txtSerialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtSerialno" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                   
                                          <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Reson For Renew/Service </label>
                                                <asp:TextBox ID="txtReson" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtReson" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Renew/Service Issue Date</label>
                                                <asp:TextBox ID="txtServiceDate" runat="server"  CssClass="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtServiceDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Condition on Renew/Service Issue</label>
                                                <asp:DropDownList ID="ddlCondition" runat="server" CssClass="form-control select2">
                                                    <asp:ListItem Value="0" Text="New" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Refursable"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Damaged"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Other"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlCondition" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Date of Receive From Renew/Service</label>
                                                <asp:TextBox ID="txtDateofRecive" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Cost of Renew/Service</label>
                                                <asp:TextBox ID="txtCostofRenewService" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Tax</label>
                                                <asp:TextBox ID="txtTax" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                          <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Renew/Service Net Amt</label>
                                                <asp:TextBox ID="txtRenewServiceNet" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                           <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Next Renew/Service Date</label>
                                                <asp:TextBox ID="txtNextServiceRenewDate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Warrenty</label>
                                                <asp:TextBox ID="txtWarrenty" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Qty</label>
                                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control"  Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                           <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Stock Qty</label>
                                                <asp:TextBox ID="txtStockQty" runat="server" CssClass="form-control" Enabled="false" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

