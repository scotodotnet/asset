﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_AssetItemRenew_Service_Approve_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        Load_Data();
    }
    private void Load_Data()
    {
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "Select (CASE WHEN IssueStatus='1' then 'RETURN'  when IssueStatus='2' then 'ISSUED' end ) as StockStatus,* from AssetItem_Renew_Service where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and (Status!='Delete' and  Status!='Approve')";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }
    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "ISSUED")
        {
            SSQL = "";
            SSQL = "Update AI set AI.Qty=(AI.Qty-AIR.IsseuQty) from MstAssetItemInventory AI inner join AssetItem_Renew_Service AIR on AI.ItemCode=AIR.ItemCode and AI.CCode=AIR.CCode and AI.LCode=AIR.LCode ";
            SSQL = SSQL + " where AIR.TransCode='" + e.CommandArgument + "'";

        }
        else if (e.CommandName == "RETURN")
        {
            SSQL = "";
            SSQL = "Update AI set AI.Qty=(AI.Qty+AIR.ReturnQty) from MstAssetItemInventory AI inner join AssetItem_Renew_Service AIR on AI.ItemCode=AIR.ItemCode and AI.CCode=AIR.CCode and AI.LCode=AIR.LCode ";
            SSQL = SSQL + " where AIR.TransCode='" + e.CommandArgument + "'";
        }
        objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "";
        SSQL = "Update AssetItem_Renew_Service set Status='Approve' where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and TransID='" + e.CommandArgument.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Transaction Approved Successfully')", true);
        Load_Data();
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Update AssetItem_Renew_Service set Status='Cancel' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and TransID='" + e.CommandArgument.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Transaction Cancel Successfully')", true);
        Load_Data();
    }
}