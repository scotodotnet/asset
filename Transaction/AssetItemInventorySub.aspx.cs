﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Transaction_AssetItemInventorySub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        if (!IsPostBack)
        {
            Load_AssetType();
            Initial_Data_Referesh_Toolkit();
            Initial_Data_Referesh_SpareParts();
            Load_Location();
            if ((string)Session["AssetItemCode"] != null)
            {
                btnSearch(sender, e);
            }    
        }
        Load_Data_ToolKit();
        Load_Data_Spareparts();
    }

    private void Load_Location()
    {
        SSQL = "";
        SSQL = "Select * from MstLocation where Ccode='" + SessionCcode + "'";
        ddlInventoryLocation.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlInventoryLocation.DataTextField = "Lcode";
        ddlInventoryLocation.DataValueField = "Lcode";
        ddlInventoryLocation.DataBind();
        ddlInventoryLocation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_AssetType()
    {
        SSQL = "";
        SSQL = "Select * from MstAssetType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlAssetType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAssetType.DataTextField = "AssetTypeName";
        ddlAssetType.DataValueField = "AssetTypeCode";
        ddlAssetType.DataBind();
        ddlAssetType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    private void btnSearch(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstAssetItemInventory where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete' and ActiveStatus='1'";
        SSQL = SSQL + " and AssetTypeCode='" + Session["AssetTypeCode"].ToString() + "' and ItemCode='" + Session["AssetItemCode"] + "' and AssetSubTypeCode='" + Session["AssetSubTypeCode"] + "' and InventoryLocationCode='" + Session["InventoryLocationCode"] + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            ddlAssetType.SelectedValue = dt.Rows[0]["AssetTypeCode"].ToString();
            ddlAssetType_SelectedIndexChanged(sender, e);
            ddlAssetSubType.SelectedValue = dt.Rows[0]["AssetSubTypeCode"].ToString();
            ddlAssetSubType_SelectedIndexChanged(sender,e);
            ddlInventoryLocation.SelectedValue = dt.Rows[0]["InventoryLocationCode"].ToString();

            txtItemCode.Enabled = false;
            txtItemCode.Text = dt.Rows[0]["ItemCode"].ToString();
            ddlItemName.SelectedValue = dt.Rows[0]["ItemCode"].ToString();
            txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            RblStatus.SelectedValue = dt.Rows[0]["ActiveStatus"].ToString();
            txtBarcode.Text = dt.Rows[0]["Barcode"].ToString();
            txtPurchaseDate.Text= dt.Rows[0]["PurchaseDate"].ToString(); 
            txtManufacture.Text= dt.Rows[0]["ManufactureName"].ToString();
            txtManufacturingDate.Text = dt.Rows[0]["ManufacturingDate"].ToString();
            txtModel.Text = dt.Rows[0]["Model"].ToString();
            txtSerialno.Text= dt.Rows[0]["SerialNo"].ToString();
            ddlCondition.SelectedValue= dt.Rows[0]["Condition"].ToString();
            txtWarrenty.Text= dt.Rows[0]["Warrenty"].ToString();
            txtQty.Text= dt.Rows[0]["Qty"].ToString();
            txtValue.Text= dt.Rows[0]["Value"].ToString();
            txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            txtAmount.Text = dt.Rows[0]["Amount"].ToString();



            txtTotAmt.Text = dt.Rows[0]["ToolkitTotalAmt"].ToString();
            txtAddOrLess.Text= dt.Rows[0]["ToolkitAddless"].ToString();
            txtNetAmt.Text= dt.Rows[0]["ToolkitNetAmt"].ToString();

            txtSpareTotalAmt.Text = dt.Rows[0]["SparePartsTotalAmt"].ToString();
            txtSpareAmtAddLess.Text = dt.Rows[0]["SparePartsAddLess"].ToString();
            txtSpareNetAmt.Text = dt.Rows[0]["SparePartsNetAmt"].ToString();

            txtTotalAssetAmt.Text = dt.Rows[0]["TotalAssetAmt"].ToString();
            txtRoundOff.Text = dt.Rows[0]["RoundOffAmt"].ToString();
            txtTotalAssetnetAmt.Text = dt.Rows[0]["AssetNetAmt"].ToString();
                                     
            SSQL = "";
            SSQL = "select * from AssetItemToolKit where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete' and ActiveStatus='1'";
            SSQL = SSQL + " and AssetTypeCode='" + Session["AssetTypeCode"].ToString() + "' and ItemCode='" + Session["AssetItemCode"] + "' and AssetSubTypeCode='" + Session["AssetSubTypeCode"] + "' ";
            DataTable dt_sub_Toolkit = new DataTable();
            dt_sub_Toolkit = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["dt_Data_Toolkit"] = dt_sub_Toolkit;
            Load_Data_ToolKit();

            SSQL = "";
            SSQL = "select * from AssetItemSpareParts where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete' and ActiveStatus='1'";
            SSQL = SSQL + " and AssetTypeCode='" + Session["AssetTypeCode"].ToString() + "' and ItemCode='" + Session["AssetItemCode"] + "' and AssetSubTypeCode='" + Session["AssetSubTypeCode"] + "' ";
            DataTable dt_sub_Spareparts = new DataTable();
            dt_sub_Spareparts = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["dt_Data_SpareParts"] = dt_sub_Spareparts;
            Load_Data_Spareparts();      

            btnSave.Text = "Update";

        }
    }
    private void Load_Data_ToolKit()
    {
        
        DataTable dt_Toolkit = new DataTable();
        dt_Toolkit = (DataTable)ViewState["dt_Data_Toolkit"];
        if (dt_Toolkit.Rows.Count > 0)
        {
            //txtTotAmt.Text= dt_Toolkit.AsEnumerable().Sum(x => x.Field<double>("Amount")).ToString();
            txtTotAmt.Text = "0.0";
            for (int i = 0; i < dt_Toolkit.Rows.Count; i++)
            {
                txtTotAmt.Text = Convert.ToDecimal(Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(dt_Toolkit.Rows[i]["Amount"].ToString())).ToString();
            }
        }

        txtNetAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) - (Convert.ToDecimal(txtAddOrLess.Text))).ToString("0.0");

        Repeater2.DataSource = dt_Toolkit;
        Repeater2.DataBind();            
       
    }
    private void Load_Data_Spareparts()
    {
        DataTable dt_Spare = new DataTable();
        dt_Spare = (DataTable)ViewState["dt_Data_SpareParts"];
        if (dt_Spare.Rows.Count > 0)
        {
            //txtSpareAmt.Text = dt_Spare.AsEnumerable().Sum(x => x.Field<decimal>("Amount")).ToString("0.0");     
            txtSpareTotalAmt.Text = "0.0";
            for (int i = 0; i < dt_Spare.Rows.Count; i++)
            {
                txtSpareTotalAmt.Text = (Convert.ToDecimal(txtSpareTotalAmt.Text) + Convert.ToDecimal(dt_Spare.Rows[i]["Amount"].ToString())).ToString();
            }
        }

        txtSpareNetAmt.Text = (Convert.ToDecimal(txtSpareTotalAmt.Text) - (Convert.ToDecimal(txtSpareAmtAddLess.Text))).ToString("0.0");

        Repeater1.DataSource = dt_Spare;
        Repeater1.DataBind();
        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        GetIPAndName getIPAndName = new GetIPAndName();
        //string _IP = getIPAndName.GetIP();
        string _IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (string.IsNullOrEmpty(_IP))
        {
            _IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        //string _HostName = getIPAndName.GetName();
        string _HostName = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
                   

        if (ddlAssetType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the AssetType Name')", true);
            return;
        }
        if (ddlAssetSubType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the AssetType Code')", true);
            return;
        }

        DataTable dt_toolkit = new DataTable();
        dt_toolkit = (DataTable)ViewState["dt_Data_Toolkit"];

        DataTable dt_SpareParts = new DataTable();
        dt_SpareParts = (DataTable)ViewState["dt_Data_SpareParts"];
       
        if (!ErrFlg)
        {
            string imgpath = "/assets/images/NoImage.png";

            if (hidd_imgPath.Value != imgpath)
            {
                imgpath = hidd_imgPath.Value.ToString();
            }

            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "update MstAssetItemInventory set AssetTypeName='" + ddlAssetType.SelectedItem.Text + "',AssetTypeCode='" + ddlAssetType.SelectedItem.Value + "'";
                SSQL = SSQL + ",AssetSubTypeCode='" + ddlAssetSubType.SelectedValue + "',AssetSubTypeName='" + ddlAssetSubType.SelectedItem.Text + "'";
                SSQL = SSQL + ",InventoryLocationCode='" + ddlInventoryLocation.SelectedValue + "' , InventoryLocationName='" + ddlInventoryLocation.SelectedItem.Text + "'";
                SSQL = SSQL + ",Imagepath='" + imgpath + "',Barcode='" + txtBarcode.Text + "',PurchaseDate='" + txtPurchaseDate.Text + "',ManufactureName='" + txtManufacture.Text + "'";
                SSQL = SSQL + ",ManufacturingDate='" + txtManufacturingDate.Text + "',Model='" + txtModel.Text + "',SerialNo='" + txtSerialno.Text + "'";
                SSQL = SSQL + ",Condition='" + ddlCondition.SelectedValue + "',Warrenty='" + txtWarrenty.Text + "',Qty='" + txtQty.Text + "',Value='" + txtValue.Text + "',Remarks='" + txtRemarks.Text + "'";
                SSQL = SSQL + ",ActiveStatus='" + RblStatus.SelectedValue + "',Amount='" + txtAmount.Text + "',ToolkitTotalAmt='" + txtTotAmt.Text + "',ToolkitAddless='" + txtAddOrLess.Text + "'";
                SSQL = SSQL + ",ToolkitNetAmt='" + txtNetAmt.Text + "',SparePartsTotalAmt='" + txtSpareTotalAmt.Text + "',SparePartsAddLess='" + txtSpareAmtAddLess.Text + "',SparePartsNetAmt='" + txtSpareNetAmt.Text + "'";
                SSQL = SSQL + ",TotalAssetAmt='" + txtTotalAssetAmt.Text + "',RoundOffAmt='" + txtRoundOff.Text + "',AssetNetAmt='" + txtTotalAssetnetAmt.Text + "'";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
                SSQL = SSQL + " and ItemCode='" + txtItemCode.Text + "' and Itemname='" + ddlItemName.SelectedItem.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstAssetItemInventory where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                SSQL = SSQL + " AssetTypeCode ='" + ddlAssetType.SelectedValue + "' and AssetSubTypeCode='" + ddlAssetSubType.SelectedValue + "'  and  Status!='Delete'";
                SSQL = SSQL + " and ItemCode='" + txtItemCode.Text + "'";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_Check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Asset TypeCode or Asset Typename is Already Present')", true);
                    return;
                }

                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "Insert into MstAssetItemInventory (Ccode,Lcode,AssetTypeCode,AssetTypeName,AssetSubTypeCode,AssetSubTypeName,InventoryLocationCode,InventoryLocationName,Imagepath,";
                    SSQL = SSQL + "ItemCode,ItemName,Barcode,PurchaseDate,ManufactureName,ManufacturingDate,Model,SerialNo,Condition,Warrenty,Qty,Value,Remarks,Amount,ToolkitTotalAmt,ToolkitAddless,";
                    SSQL = SSQL + "ToolkitNetAmt,SparePartsTotalAmt,SparePartsAddLess,SparePartsNetAmt,TotalAssetAmt,RoundOffAmt,AssetNetAmt,";
                    SSQL = SSQL + "CreatedOn,Host_IP,Host_Name,UserName,UserRole,Status,ActiveStatus)";
                    SSQL = SSQL + "values('" + SessionCcode + "','" + SessionLcode + "','" + ddlAssetType.SelectedValue + "','" + ddlAssetType.SelectedItem.Text + "','" + ddlAssetSubType.SelectedValue + "'";
                    SSQL = SSQL + ",'" + ddlAssetSubType.SelectedItem.Text + "','" + ddlInventoryLocation.SelectedValue + "','" + ddlInventoryLocation.SelectedItem.Text + "','" + imgpath + "'";
                    SSQL = SSQL + ",'" + txtItemCode.Text + "','" + ddlItemName.SelectedItem.Text + "','" + txtBarcode.Text + "','" + txtPurchaseDate.Text + "','" + txtManufacture.Text + "'";
                    SSQL = SSQL + ",'" + txtManufacturingDate.Text + "','" + txtModel.Text + "','" + txtSerialno.Text + "','" + ddlCondition.SelectedValue + "','" + txtWarrenty.Text + "'";
                    SSQL = SSQL + ",'" + txtQty.Text + "','" + txtValue.Text + "','" + txtRemarks.Text + "','" + txtAmount.Text + "','" + txtTotAmt.Text + "','" + txtAddOrLess.Text + "','" + txtNetAmt.Text + "'";
                    SSQL = SSQL + ",'" + txtSpareTotalAmt.Text + "','" + txtSpareAmtAddLess.Text + "','" + txtSpareNetAmt.Text + "','" + txtTotalAssetAmt.Text + "','" + txtRoundOff.Text + "','" + txtTotalAssetnetAmt.Text + "'";
                    SSQL = SSQL + " ,Convert(varchar,getdate(),105),'" + _IP + "','" + _HostName + "','" + Session["UserId"] + "','" + Session["Isadmin"] + "','Add','" + RblStatus.SelectedValue + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            if (dt_toolkit.Rows.Count > 0)
            {
                SSQL = "";
                SSQL = "Delete from AssetItemToolkit where ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AssettypeCode='" + ddlAssetType.SelectedValue + "' and  Status!='Delete'";
                SSQL = SSQL + " and AssetSubtypeCode='" + ddlAssetSubType.SelectedValue + "' and ItemCode='" + txtItemCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                for (int i = 0; i < dt_toolkit.Rows.Count; i++)
                {
                    SSQL = "";
                    SSQL = "Insert into AssetItemToolkit (CCode,Lcode,AssetTypeCode,AssetTypeName,AssetSubtypeCode,AssetSubTypeName,ItemCode,ItemName,Toolkit_Name,Qty,Value,Amount,CreatedOn,Host_IP,Host_Name,UserName,UserRole,Status,ActiveStatus)";
                    SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlAssetType.SelectedValue + "','" + ddlAssetType.SelectedItem.Text + "',";
                    SSQL = SSQL + " '" + ddlAssetSubType.SelectedValue + "','" + ddlAssetSubType.SelectedItem.Text + "','" + txtItemCode.Text + "','" + ddlItemName.SelectedItem.Text + "',";
                    SSQL = SSQL + " '" + dt_toolkit.Rows[i]["Toolkit_Name"].ToString() + "','" + dt_toolkit.Rows[i]["Qty"].ToString() + "','" + dt_toolkit.Rows[i]["Value"].ToString() + "','" + dt_toolkit.Rows[i]["Amount"].ToString() + "',";
                    SSQL = SSQL + " convert(varchar,getdate(),105),'" + _IP + "','" + _HostName + "','" + Session["UserId"] + "','" + Session["Isadmin"] + "','Add','" + dt_toolkit.Rows[i]["ActiveStatus"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            if (dt_SpareParts.Rows.Count > 0)
            {
                SSQL = "";
                SSQL = "Delete from AssetItemSpareParts where ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AssettypeCode='" + ddlAssetType.SelectedValue + "' and  Status!='Delete'";
                SSQL = SSQL + " and AssetSubtypeCode='" + ddlAssetSubType.SelectedValue + "' and ItemCode='" + txtItemCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                for (int i = 0; i < dt_SpareParts.Rows.Count; i++)
                {
                    SSQL = "";
                    SSQL = "Insert into AssetItemSpareParts (CCode,Lcode,AssetTypeCode,AssetTypeName,AssetSubtypeCode,AssetSubTypeName,ItemCode,ItemName,SparePartsName,Qty,Value,Amount,CreatedOn,Host_IP,Host_Name,UserName,UserRole,Status,ActiveStatus)";
                    SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlAssetType.SelectedValue + "','" + ddlAssetType.SelectedItem.Text + "',";
                    SSQL = SSQL + " '" + ddlAssetSubType.SelectedValue + "','" + ddlAssetSubType.SelectedItem.Text + "','" + txtItemCode.Text + "','" + ddlItemName.SelectedItem.Text + "',";
                    SSQL = SSQL + " '" + dt_SpareParts.Rows[i]["SparePartsName"].ToString() + "','" + dt_SpareParts.Rows[i]["Qty"].ToString() + "','" + dt_SpareParts.Rows[i]["Value"].ToString() + "','" + dt_SpareParts.Rows[i]["Amount"].ToString() + "',";
                    SSQL = SSQL + " convert(varchar,getdate(),105),'" + _IP + "','" + _HostName + "','" + Session["UserId"] + "','" + Session["Isadmin"] + "','Add','" + dt_SpareParts.Rows[i]["ActiveStatus"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            if (!ErrFlg)
            {
                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset Inventory Added Saved Successfully')", true);
                    btnClear_Click(sender, e);
                }
                else if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset Inventory Updated Successfully')", true);
                    btnClear_Click(sender, e);
                }
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

        txtItemCode.Text = "";
        ddlAssetSubType.ClearSelection();
        ddlAssetType.ClearSelection();
        ddlItemName.ClearSelection();
        ddlInventoryLocation.ClearSelection();
        txtBarcode.Text = "";
        txtPurchaseDate.Text = "";
        txtManufacture.Text = "";
        txtManufacturingDate.Text = "";
        txtModel.Text = "";
        txtSerialno.Text = "";
        txtWarrenty.Text = "";
        ddlCondition.ClearSelection();
        txtQty.Text = "0";
        txtValue.Text = "0";
        RblStatus.ClearSelection();
        txtRemarks.Text = "";
        btnSave.Text = "Save";
        txtTotalAssetAmt.Text = "0";
        txtRoundOff.Text = "0";
        txtNetAmt.Text = "0";
        txtToolkitAmount.Text = "0";
        txtToolKitName.Text = "";
        txtToolkitQty.Text = "0";
        txtToolKitValue.Text = "0";
        txtToolkitAmount.Text = "0";
        txtSpareAmt.Text = "0";
        txtAddOrLess.Text = "0";
        txtAddOrLess_TextChanged(sender, e);
        txtSparePartsName.Text = "";
        txtSpareQty.Text="0";
        txtSpareValue.Text = "0";
        txtSpareTotalAmt.Text = "0";
        txtSpareAmtAddLess.Text = "0";
        txtSpareNetAmt.Text = "0";
        rblToolkitStatus.ClearSelection();
        rblSparePartsStatus.ClearSelection();
        txtAmount.Text = "0";
        txtTotalAssetnetAmt.Text = "0";
        txtTotAmt.Text = "0";
        txtNetAmt.Text = "0";
                                        
        Initial_Data_Referesh_Toolkit();
        Initial_Data_Referesh_SpareParts();
    }
    private void Initial_Data_Referesh_Toolkit()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Toolkit_Name");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Value");
        dt.Columns.Add("Amount");
        dt.Columns.Add("ActiveStatus");
        ViewState["dt_Data_Toolkit"] = dt;
        Load_Data_ToolKit();
    }
    private void Initial_Data_Referesh_SpareParts()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("SparePartsName");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Value");
        dt.Columns.Add("Amount");
        dt.Columns.Add("ActiveStatus");
        ViewState["dt_Data_SpareParts"] = dt;
        Load_Data_Spareparts();
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Master/AssetItemInventoryMain.aspx");
    }

    protected void btnAddtoolkit_Click(object sender, EventArgs e)
    {
            
        if (ddlAssetType.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the AssetType Name')", true);
            return;
        }
        if (ddlAssetSubType.SelectedItem.Text == null || ddlAssetSubType.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select AssetSubTypeName')", true);
            return;
        }
        if (ddlInventoryLocation.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Inventory Location')", true);
            return;
        }
        if (txtToolKitName.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Toolkit Name')", true);
            return;
        }
        if (txtToolkitQty.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Toolkit Qty')", true);
            return;
        }
        if (txtToolKitValue.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Toolkit value')", true);
            return;
        }
        if (txtToolkitAmount.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Toolkit Amount')", true);
            return;
        }
        DataTable dt_RowAdd = new DataTable();
        dt_RowAdd = (DataTable)(ViewState["dt_Data_Toolkit"]);
        DataRow dr;

        if (!ErrFlg)
        {
            if (dt_RowAdd.Rows.Count > 0)
            {
                //SSQL = "";
                //SSQL = "Select * from AssetItemToolkit where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                //SSQL = SSQL + " and Toolkit_Name='" + txtToolKitName.Text + "' and ItemCode='" + txtItemCode.Text + "' and ItemName='" + ddlItemName.SelectedItem.Text + "' and Status!='Delete'";
                //SSQL = SSQL + " and AssetTypeCode='" + ddlAssetType.SelectedValue + "' and AssetSubTypeCode='" + ddlAssetSubType.SelectedValue + "'";
                //DataTable dt_check = new DataTable();
                //dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dt_check.Rows.Count > 0)
                //{
                //    ErrFlg = true;
                //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Item Toolkit Already Present...')", true);
                //    return;
                //}
                if (!ErrFlg)
                {
                    for (int i = 0; i < dt_RowAdd.Rows.Count; i++)
                    {
                        if (txtToolKitName.Text==dt_RowAdd.Rows[i]["Toolkit_Name"].ToString())
                        {
                            ErrFlg = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Item Toolkit Already Present...')", true);
                            break;
                        }
                    }
                }
                if (!ErrFlg)
                {
                    dr = dt_RowAdd.NewRow();
                    dr["ItemCode"] = txtItemCode.Text;
                    dr["Toolkit_Name"] = txtToolKitName.Text;
                    dr["Qty"] = txtToolkitQty.Text;
                    dr["Value"] = txtToolKitValue.Text;
                    dr["Amount"] = txtToolkitAmount.Text;
                    dr["ActiveStatus"] = rblToolkitStatus.SelectedValue;    
                    dt_RowAdd.Rows.Add(dr);    
                }
            }
            else
            {
                dr = dt_RowAdd.NewRow();
                dr["ItemCode"] = txtItemCode.Text;
                dr["Toolkit_Name"] = txtToolKitName.Text;
                dr["Qty"] = txtToolkitQty.Text;
                dr["Value"] = txtToolKitValue.Text;
                dr["Amount"] = txtToolkitAmount.Text;
                dr["ActiveStatus"] = rblToolkitStatus.SelectedValue;
                dt_RowAdd.Rows.Add(dr);
            }

            ViewState["dt_Data_Toolkit"] = dt_RowAdd;
            Load_Data_ToolKit();
            Load_Total();
        }
    }
    protected void btnToolkitDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        string Toolkit_Name = e.CommandName.ToString();
    
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["dt_Data_Toolkit"];

        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = dt.Rows[i];

            if (dr["Toolkit_Name"].ToString() == Toolkit_Name)
                dr.Delete();
        }
        dt.AcceptChanges();
        ViewState["dt_Data_Toolkit"] = dt;
        Load_Data_ToolKit();
        Load_Total();
    }

    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txtAddOrLess_TextChanged(object sender, EventArgs e)
    {
        txtNetAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) - Convert.ToDecimal(txtAddOrLess.Text)).ToString();
    }

    protected void btnSpareAdd_Click(object sender, EventArgs e)
    {
        if (ddlAssetSubType.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select AssetSubTypeName')", true);
            return;
        }

        if (ddlAssetType.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the AssetType Name')", true);
            return;
        }
        if (ddlInventoryLocation.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Inventory Location')", true);
            return;
        }
        if (txtSparePartsName.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Spareparts Name')", true);
            return;
        }
        if (txtSpareQty.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Spareparts Qty')", true);
            return;
        }
        if (txtSpareValue.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Spareparts value')", true);
            return;
        }
        if (txtSpareAmt.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Spareparts Amount')", true);
            return;
        }
        DataTable dt_RowAdd = new DataTable();
        dt_RowAdd = (DataTable)(ViewState["dt_Data_SpareParts"]);
        DataRow dr;

        if (!ErrFlg)
        {
            if (dt_RowAdd.Rows.Count > 0)
            {
                //SSQL = "";
                //SSQL = "Select * from AssetItemSpareparts where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                //SSQL = SSQL + " and SparePartsName='" + txtSparePartsName.Text + "' and (ItemCode='" + txtItemCode.Text + "' or ItemName='" + ddlItemName.SelectedItem.Text + "') and Status!='Delete'";
                //DataTable dt_check = new DataTable();
                //dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dt_check.Rows.Count > 0)
                //{
                //    ErrFlg = true;
                //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Item Spare parts Already Present...')", true);
                //    return;
                //}
                if (!ErrFlg)
                {
                    for (int i = 0; i < dt_RowAdd.Rows.Count; i++)
                    {
                        if (txtSparePartsName.Text == dt_RowAdd.Rows[i]["SparePartsName"].ToString())
                        {
                            ErrFlg = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Item Spareparts Already Present...')", true);
                            break;
                        }
                    }
                }
                if (!ErrFlg)
                {
                    dr = dt_RowAdd.NewRow();
                    dr["ItemCode"] = txtItemCode.Text;
                    dr["SparePartsName"] = txtSparePartsName.Text;
                    dr["Qty"] = txtSpareQty.Text;
                    dr["Value"] = txtSpareValue.Text;
                    dr["Amount"] = txtSpareAmt.Text;
                    dr["ActiveStatus"] = rblSparePartsStatus.SelectedValue;
                    dt_RowAdd.Rows.Add(dr);                                      
                }
            }
            else
            {
                dr = dt_RowAdd.NewRow();
                dr["ItemCode"] = txtItemCode.Text;
                dr["SparePartsName"] = txtSparePartsName.Text;
                dr["Qty"] = txtSpareQty.Text;
                dr["Value"] = txtSpareValue.Text;
                dr["Amount"] = txtSpareAmt.Text;
                dr["ActiveStatus"] = rblSparePartsStatus.SelectedValue;
                dt_RowAdd.Rows.Add(dr);
            }

            ViewState["dt_Data_SpareParts"] = dt_RowAdd;
            Load_Data_Spareparts();
            Load_Total();
        }
    }

    protected void txtSpareDiscount_TextChanged(object sender, EventArgs e)
    {
        
    }

    protected void txtSpareAmtAddLess_TextChanged(object sender, EventArgs e)
    {
        txtSpareNetAmt.Text = (Convert.ToDecimal(txtSpareTotalAmt.Text) - (Convert.ToDecimal(txtSpareAmtAddLess.Text))).ToString();
    }

    protected void ddlAssetType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_AssetSubType(ddlAssetType.SelectedValue);
    }
    private void Load_AssetSubType(string selectedValue)
    {
        SSQL = "";
        SSQL = "Select * from MstAssetSubType where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
        SSQL = SSQL + " and AssetTypeCode='" + selectedValue + "'";
        ddlAssetSubType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAssetSubType.DataTextField = "AssetSubTypeName";
        ddlAssetSubType.DataValueField = "AssetSubTypeCode";
        ddlAssetSubType.DataBind();
        ddlAssetSubType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void btnSparePartsDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["dt_Data_SpareParts"];

        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = dt.Rows[i];

            if (dr["SparePartsName"].ToString() == e.CommandName.ToString())
                dr.Delete();
        }
        dt.AcceptChanges();
        ViewState["dt_Data_SpareParts"] = dt;
        Load_Data_Spareparts();
        Load_Total();
    }

    protected void ddlAssetSubType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstAssetItem where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete' and ActiveStatus='1'";
        SSQL = SSQL + " and AssetTypeCode='" + ddlAssetType.SelectedValue + "' and AssetSubTypeCode='" + ddlAssetSubType.SelectedValue + "'";
        ddlItemName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "itemCode";
        ddlItemName.DataBind();
        ddlItemName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtItemCode.Text = ddlItemName.SelectedValue;
    }

    protected void txtQty_TextChanged(object sender, EventArgs e)
    {          
        Load_Total();
    }

    private void Load_Total()
    {
        txtAmount.Text = (Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtValue.Text)).ToString("0.0");

        txtToolkitAmount.Text = (Convert.ToDecimal(txtToolkitQty.Text) * Convert.ToDecimal(txtToolKitValue.Text)).ToString("0.0");

        txtSpareAmt.Text = (Convert.ToDecimal(txtSpareQty.Text) * Convert.ToDecimal(txtSpareValue.Text)).ToString("0.0");

        DataTable dt_Toolkit = new DataTable();
        dt_Toolkit = (DataTable)ViewState["dt_Data_Toolkit"];
        if (dt_Toolkit.Rows.Count > 0)
        {
            txtTotAmt.Text = "0.0";
            //txtTotAmt.Text= dt_Toolkit.AsEnumerable().Sum(x => x.Field<double>("Amount")).ToString();
            for (int i = 0; i < dt_Toolkit.Rows.Count; i++)
            {
                txtTotAmt.Text = Convert.ToDecimal(Convert.ToDecimal(txtTotAmt.Text)+Convert.ToDecimal(dt_Toolkit.Rows[i]["Amount"].ToString())).ToString();
            }
        }

        txtNetAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) - (Convert.ToDecimal(txtAddOrLess.Text))).ToString("0.0");

        DataTable dt_Spare = new DataTable();
        dt_Spare = (DataTable)ViewState["dt_Data_SpareParts"];
        if (dt_Spare.Rows.Count > 0)
        {
            //txtSpareAmt.Text = dt_Spare.AsEnumerable().Sum(x => x.Field<decimal>("Amount")).ToString("0.0");      
            txtSpareTotalAmt.Text = "0.0";
            for (int i = 0; i < dt_Spare.Rows.Count; i++)
            {
                txtSpareTotalAmt.Text = (Convert.ToDecimal(txtSpareTotalAmt.Text) + Convert.ToDecimal(dt_Spare.Rows[i]["Amount"].ToString())).ToString();
            }
        }

        txtSpareNetAmt.Text = (Convert.ToDecimal(txtSpareTotalAmt.Text) - (Convert.ToDecimal(txtSpareAmtAddLess.Text))).ToString("0.0");

        txtTotalAssetAmt.Text = (Convert.ToDecimal(txtAmount.Text) + Convert.ToDecimal(txtNetAmt.Text) + Convert.ToDecimal(txtSpareNetAmt.Text)).ToString("0.0");

        txtTotalAssetnetAmt.Text=Math.Round(Convert.ToDecimal(txtTotalAssetAmt.Text),2,MidpointRounding.AwayFromZero).ToString("0.0");

        txtRoundOff.Text = (Convert.ToDecimal(txtTotalAssetAmt.Text) - Convert.ToDecimal(txtTotalAssetnetAmt.Text)).ToString("0.0");


    }

    protected void txtValue_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtAmount_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtToolkitQty_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtToolKitValue_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtToolkitAmount_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtTotAmt_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }
          
    protected void txtSpareTotalAmt_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtTotalAssetAmt_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtRoundOff_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtTotalAssetnetAmt_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtSpareQty_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtSpareValue_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }

    protected void txtSpareAmt_TextChanged(object sender, EventArgs e)
    {
        Load_Total();
    }
}