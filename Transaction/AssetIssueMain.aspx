﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AssetIssueMain.aspx.cs" Inherits="Transaction_MstAssetIssueMain" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span>Asset Item List</span></h3>
                                    <div class="box-tools pull-right">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">
                                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>S. No</th>
                                                                <th>Transaction ID</th>
                                                                <th>Transaction Date</th>
                                                                <th>Item Name</th>
                                                                  <th>Last Transanction To</th>
                                                                <th>Last Transanction by</th>
                                                                 <th>Issue/Return</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("TransID")%></td>
                                                        <td><%# Eval("TransDate")%></td>
                                                        <td><%# Eval("ItemName")%></td>
                                                        <td><%# Eval("CheckOUTCode")+" "+Eval("CheckINCode")%></td>
                                                        <td><%# Eval("CheckOUTByCode")+" "+Eval("CheckINByCode")%></td>
                                                        <td><%# Eval("StockStatus")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="btnEditGrid_Command" CommandArgument='<%#Eval("TransID")%>' CommandName="Edit">
                                                    </asp:LinkButton>

                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" CommandArgument='<%#Eval("TransID") %>' OnCommand="btnDeleteGrid_Command" CommandName="Delete"
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Transaction?');">
                                                    </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>

                                        </div>
                                        <!-- /.table -->
                                    </div>
                                    <!-- /.mail-box-messages -->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

