﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Transaction_MstAssetIssueMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        Load_Data();
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "Select (CASE WHEN IssueStatus='1' then 'RETURN'  when IssueStatus='2' then 'ISSUED' end ) as StockStatus,* from AssetItemIssue_Return where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {

        SSQL = "";
        SSQL = "Select * from AssetItemIssue_Return where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and TransID='" + e.CommandArgument.ToString() + "' and (Status='Approve' )";
        DataTable dt_CheckStatus = new DataTable();
        dt_CheckStatus = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_CheckStatus.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Transaction is Already Approved')", true);
        }
        else
        {
            SSQL = "";
            SSQL = "";
            SSQL = "Select * from AssetItemIssue_Return where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and TransID='" + e.CommandArgument.ToString() + "' and Status='Cancel'";

            dt_CheckStatus = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_CheckStatus.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Transaction is Already Cancel')", true);
            }
            else
            {
                Session["TransID"] = e.CommandArgument.ToString();
                Response.Redirect("/Transaction/AssetIssueSub.aspx");
            }
        }
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from AssetItemIssue_Return where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and TransID='" + e.CommandArgument.ToString() + "' and (Status='Approve' )";
        DataTable dt_CheckStatus = new DataTable();
        dt_CheckStatus = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_CheckStatus.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Transaction is Already Approved')", true);
        }
        else
        {

            SSQL = "";
            SSQL = "";
            SSQL = "Select * from AssetItemIssue_Return where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and TransID='" + e.CommandArgument.ToString() + "' and Status='Cancel'";

            dt_CheckStatus = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_CheckStatus.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Transaction is Already Cancel')", true);
            }
            else
            {
                SSQL = "";
                SSQL = "Update AssetItemIssue_Return set Status='Delete' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and TransID='" + e.CommandArgument.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Transaction is Deleted Successfully')", true);
            }
        }
        Load_Data();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");           
        Response.Redirect("/Transaction/AssetIssueSub.aspx");
    }
}